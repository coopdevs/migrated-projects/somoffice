import pytest


@pytest.fixture
def partner_fixture(mocker):
    partner_mock = mocker.Mock(spec=[
        "name",
        "firstname",
        "phone",
        "sponsorship_code",
        "sponsees_number",
        "sponsees_max"
    ])
    partner_mock.name = "Name Lastname"
    partner_mock.firstname = "Name"
    partner_mock.phone = "666666666"
    partner_mock.sponsorship_code = "jblbp"
    partner_mock.sponsees_number = 3
    partner_mock.sponsees_max = 5
    partner_mock.member = True
    return partner_mock


@pytest.fixture
def current_user_fixture(mocker):
    current_user_mock = mocker.Mock(spec=["username", "profile"])
    current_user_mock.username = "username"
    current_user_mock.profile = mocker.Mock(spec=["preferredLocale"])
    current_user_mock.profile.preferredLocale = "ca"
    current_user_mock.profile.remoteId = "1234"
    return current_user_mock


@pytest.fixture
def keycloak_user_fixture(mocker):
    return {
        "email": "fake@email.coop",
    }
