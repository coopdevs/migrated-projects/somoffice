from somconnexio.providers.profile import ProfileProvider


class TestCaseProfileProvider:
    def test_get_resource(self, mocker, current_user_fixture, partner_fixture, keycloak_user_fixture):
        KeycloakRemoteUserBackendMock = mocker.patch(
            'somconnexio.providers.profile.KeycloakRemoteUserBackend',
            return_value=mocker.Mock(spec=["get_keycloak_user"]),
        )
        KeycloakRemoteUserBackendMock.return_value.get_keycloak_user.return_value = keycloak_user_fixture
        PartnerMock = mocker.patch(
            'somconnexio.providers.profile.Partner',
            return_value=mocker.Mock(spec_set=["get"]),
        )
        PartnerMock.return_value.get.return_value = partner_fixture
        mocker.patch(
            'somconnexio.providers.profile.get_partner_resource',
            return_value=partner_fixture,
        )
        mocker.patch(
            'somconnexio.providers.profile.get_customer_resource',
        )

        options = {
            "remote_user_id": "1234",
            "current_user": current_user_fixture,
        }

        profile = ProfileProvider(options).get_resource()

        sponsor_info = profile["sponsorship_information"]

        assert profile["full_name"] == "Name Lastname"
        assert profile["first_name"] == "Name"
        assert profile["username"] == "username"
        assert profile["preferred_locale"] == "ca"
        assert profile["email"] == "fake@email.coop"
        assert profile["role"] == "member"
        assert profile["phone"] == "666666666"
        assert sponsor_info["sponsorship_code"] == "jblbp"
        assert sponsor_info["sponsees_number"] == 3
        assert sponsor_info["sponsees_max"] == 5
