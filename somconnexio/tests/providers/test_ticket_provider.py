import pytest

from somconnexio.providers.tickets import TicketsProvider

class FakeDynamicField:
    @property
    def value(self):
        return 1


class FakeResponse:
    def dynamic_field_get(self, code):
        return FakeDynamicField()


class FakeTicket:
    @property
    def id(self):
        return 123

    @property
    def response(self):
        return FakeResponse()


class FakeProfile:
    @property
    def remoteId(self):
        return "2"


class FakeUser:
    @property
    def profile(self):
        return FakeProfile()


class TestTicketsProvider:
    def test_create_ok(self, mocker):
        mocker.patch(
            "somconnexio.providers.tickets.OTRSClient",
            return_value=mocker.Mock()
        )

        build_ticket_mock = mocker.patch(
            "somconnexio.providers.tickets.build_ticket",
            return_value=mocker.Mock()
        )

        user = FakeUser()
        options = {
            "user": user
        }
        meta = [{
            "key": "ticket_type",
            "value": "report_error",
        }]

        result = TicketsProvider(options).create(
            user=user,
            meta=meta
        )

        assert result
        build_ticket_mock.assert_called_once_with(
            "report_error",
            user,
            {
                "ticket_type": "report_error",
            },
            []
        )
        build_ticket_mock.return_value.create.assert_called_once_with()

    def test_create_ok_with_override_ticket_ids(self, mocker):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.tickets.OTRSClient",
            return_value=mocker.Mock()
        )

        build_ticket_mock = mocker.patch(
            "somconnexio.providers.tickets.build_ticket",
            return_value=mocker.Mock())

        user = FakeUser()
        options = {
            "user": user
        }
        meta = [{
            "key": "ticket_type",
            "value": "change_tariff",
        }]

        override_ticket_ids = [1, 2]

        def validate_secure_id_side_effect(user, id):
            if id == 1:
                return {
                    "resource_id": 4
                }
            elif id == 2:
                return {
                    "resource_id": 5
                }

        validate_secure_id_mock = mocker.patch(
            "somconnexio.providers.tickets.validate_secure_id",
            side_effect=validate_secure_id_side_effect
        )

        result = TicketsProvider(options).create(
            user=user,
            meta=meta,
            override_ticket_ids=override_ticket_ids
        )

        expected_validate_secure_id_calls = [
            mocker.call(user, 1),
            mocker.call(user, 2),
        ]

        validate_secure_id_mock.assert_has_calls(
            expected_validate_secure_id_calls
        )

        expected_ticker_update_calls = [
            mocker.call(4, State="closed successful"),
            mocker.call(5, State="closed successful"),
        ]

        OTRSClientMock.return_value.client.ticket_update.assert_has_calls(
                expected_ticker_update_calls
        )

        assert result
        build_ticket_mock.assert_called_once_with(
            "change_tariff",
            user,
            {
                "ticket_type": "change_tariff",
            },
            override_ticket_ids
        )
        build_ticket_mock.return_value.create.assert_called_once_with()

    def test_create_raise_invalid_ticket_id(self, mocker):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.tickets.OTRSClient",
            return_value=mocker.Mock()
        )

        build_ticket_mock = mocker.patch(
            "somconnexio.providers.tickets.build_ticket",
            return_value=mocker.Mock())

        user = FakeUser()
        options = {
            "user": user
        }
        meta = [{
            "key": "ticket_type",
            "value": "change_tariff",
        }]

        override_ticket_ids = [1, 2]

        validate_secure_id_mock = mocker.patch(
            "somconnexio.providers.tickets.validate_secure_id",
            return_value={"resource_id": None},
        )

        with pytest.raises(ValueError) as e_info:
            result = TicketsProvider(options).create(
                user=user,
                meta=meta,
                override_ticket_ids=override_ticket_ids
            )
            assert e_info.message == "Invalid ticket id"

    def test_get_resources_ok(self, mocker):
        expected_tickets = [FakeTicket()]
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.tickets.OTRSClient",
            return_value=mocker.Mock(spec_set=["search_tickets"])
        )
        OTRSClientMock.return_value.search_tickets.return_value = expected_tickets
        build_ticket_mock = mocker.patch(
            "somconnexio.providers.tickets.build_ticket",
            return_value=mocker.Mock(),
        )
        build_ticket_mock.return_value.get_search_args.return_value = {}

        build_secure_id_generator_mock = mocker.patch(
            "somconnexio.providers.tickets.build_secure_id_generator",
            return_value=mocker.Mock(),
        )
        build_secure_id_generator_mock.return_value.return_value = "1234"

        user = FakeUser()
        options = {
            "user": user
        }

        ticket_type = "report_error"
        filters = []

        result = TicketsProvider(options).get_resources(
            ticket_type=ticket_type,
            filters=filters,
        )

        assert result == [{"id": "1234", "meta": [{"key": "new_product_code", "value": 1}]}]
        build_ticket_mock.assert_called_once_with(
            "report_error",
            user,
            {},
        )
        build_ticket_mock.return_value.get_search_args.assert_called_once_with()
        OTRSClientMock.return_value.search_tickets.assert_called_once_with(
            **build_ticket_mock.return_value.get_search_args.return_value,
            CustomerID=user.profile.remoteId
        )
