import pytest

from pyotrs.lib import DynamicField

from somconnexio.providers.ticket_types.add_data_ticket import AddDataTicket


class TestCaseAddDataTicket:
    def test_create(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.ticket_types.base_ticket.OTRSClient",
            return_value=mocker.Mock()
        )
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())
        KeycloakRemoteUserBackendMock = mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())
        translationMock = mocker.patch("somconnexio.providers.ticket_types.add_data_ticket.translation", return_value=mocker.Mock())
        translationMock.get_language.return_value = "ca"

        expected_ticket_data = {
            "Title": "Sol·licitud abonament addicional oficina virtual",
            "Queue": "Oficina Virtual::Dades addicionals::Rebut Dades",
            "State": AddDataTicket.TICKET_STATE,
            "Type": AddDataTicket.TICKET_TYPE,
            "Priority": AddDataTicket.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_article_data = {
            "Subject": "Sol·licitud abonament addicional oficina virtual",
            "Body": "-"
        }

        fields_dict = {
            "phone_number": "666666666",
            "new_product_code": "PRODUCT_CODE",
            "subscription_email": "fakeemail@email.coop",
        }

        AddDataTicket(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        calls = [
            mocker.call('ProcessManagementProcessID', 'Process-7f03577ba04a39dbc9e0fe0d2c6144df'),
            mocker.call('ProcessManagementActivityID', 'Activity-d621f2e21d7583c0dffc7569cd3c9f59'),
            mocker.call('liniaMobil', '666666666'),
            mocker.call('productAbonamentDadesAddicionals', 'PRODUCT_CODE'),
            # TODO: Why are we using correuElectronic in this ticket?
            mocker.call('correuElectronic', 'fakeemail@email.coop'),
            mocker.call('idioma', 'ca_ES')
        ]
        DynamicFieldMock.assert_has_calls(calls)
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[mocker.ANY for call in calls]
        )
