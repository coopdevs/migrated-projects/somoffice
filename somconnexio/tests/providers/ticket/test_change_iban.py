from somconnexio.providers.ticket_types.change_iban_ticket import \
    ChangeIbanTicket


class TestCaseChangeIbanTicket:
    def test_create(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.ticket_types.base_ticket.OTRSClient",
            return_value=mocker.Mock()
        )
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())  # noqa
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())  # noqa
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())  # noqa
        mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())  # noqa
        UUIDMock = mocker.patch("somconnexio.providers.ticket_types.base_change_ticket.uuid", spec=['uuid4'])  # noqa
        UUIDMock.uuid4.return_value = "UUID-token"

        expected_ticket_data = {
            "Title": "Sol·licitud canvi de IBAN oficina virtual",
            "Queue": "Oficina Virtual::Canvi IBAN contractes::Rebut",
            "State": ChangeIbanTicket.TICKET_STATE,
            "Type": ChangeIbanTicket.TICKET_TYPE,
            "Priority": ChangeIbanTicket.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_message = "-"
        expected_article_data = {
            "Subject": "Sol·licitud canvi de IBAN oficina virtual",
            "Body": expected_message
        }

        fields_dict = {
            "new_value": "ES6621000418401234567891",
            "scope": "all",
            "selected_subscriptions": ["1254"],
        }

        ChangeIbanTicket(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        expected_df_calls = [
            mocker.call("nouIBAN", fields_dict["new_value"]),
            mocker.call('ProcessManagementProcessID', 'Process-76f712cb8936190743ace3a886b3d167'),  # noqa
            mocker.call('ProcessManagementActivityID', 'Activity-621e28c842e6556964d4a6ce04286235'),  # noqa
            mocker.call('IDOV', UUIDMock.uuid4.return_value),
            mocker.call('refOdooContract', "1254"),
        ]
        for call in DynamicFieldMock.mock_calls:
            assert call in expected_df_calls
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[
                mocker.ANY for c in expected_df_calls
            ]
        )
