import pytest

from somconnexio.providers.tickets import build_ticket
from somconnexio.providers.ticket_types.report_error_ticket import ReportErrorTicket
from somconnexio.providers.ticket_types.change_iban_ticket import ChangeIbanTicket
from somconnexio.providers.ticket_types.change_email_ticket import ChangeEmailTicket
from somconnexio.providers.ticket_types.add_data_ticket import AddDataTicket
from somconnexio.providers.ticket_types.change_tariff_ticket import ChangeTariffTicket
from somconnexio.providers.ticket_types.change_personal_email_ticket import ChangePersonalEmailTicket
from somconnexio.providers.ticket_types.change_tariff_adsl_ticket import ChangeTariffTicketADSL
from somconnexio.providers.ticket_types.change_tariff_fiber_ticket import ChangeTariffTicketFiber


class TestCaseBuildTicket:
    def _execute_and_assert(self, ticket_type, expected_ticket_class, fields=object):
        ticket = build_ticket(
                ticket_type,
                object,
                fields
            )
        assert ticket.__class__ == expected_ticket_class

    def test_build_report_ticket(self):
        ticket_type = "report_error"
        expected_ticket_class = ReportErrorTicket

        self._execute_and_assert(ticket_type, expected_ticket_class)

    def test_build_change_iban(self):
        ticket_type = "change_iban"
        expected_ticket_class = ChangeIbanTicket

        self._execute_and_assert(ticket_type, expected_ticket_class)

    def test_build_change_email(self):
        ticket_type = "change_email"
        expected_ticket_class = ChangeEmailTicket

        self._execute_and_assert(ticket_type, expected_ticket_class)

    def test_build_add_data(self):
        ticket_type = "additional_data"
        expected_ticket_class = AddDataTicket

        self._execute_and_assert(ticket_type, expected_ticket_class)

    def test_build_change_tariff(self):
        ticket_type = "change_tariff"
        expected_ticket_class = ChangeTariffTicket

        self._execute_and_assert(ticket_type, expected_ticket_class)

    def test_build_change_personal_email(self):
        ticket_type = "change_personal_email"
        expected_ticket_class = ChangePersonalEmailTicket

        self._execute_and_assert(ticket_type, expected_ticket_class)

    def test_build_change_tariff_fiber(self):
        ticket_type = "change_tariff_out_landline"
        expected_ticket_class = ChangeTariffTicketFiber
        fields = {
            "previous_service": "SE_SC_REC_BA_FIBRA",
        }

        self._execute_and_assert(ticket_type, expected_ticket_class, fields)

    def test_build_change_tariff_adsl(self):
        ticket_type = "change_tariff_out_landline"
        expected_ticket_class = ChangeTariffTicketADSL
        fields = {
            "previous_service": "SE_SC_REC_BA_ADSL_SF",
        }

        self._execute_and_assert(ticket_type, expected_ticket_class, fields)

    def test_build_unknown_ticket_type(self):
        ticket_type = "unknown_type"
        with pytest.raises(ValueError) as error:
            build_ticket(
                ticket_type,
                object,
                object
            )
            assert error == "Unknown ticket type: unknown_type"
