from somconnexio.providers.ticket_types.change_email_ticket import \
    ChangeEmailTicket


class TestCaseChangeEmailTicket:
    def test_create(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.ticket_types.base_ticket.OTRSClient",
            return_value=mocker.Mock()
        )
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())  # noqa
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())  # noqa
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())  # noqa
        mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())  # noqa
        UUIDMock = mocker.patch("somconnexio.providers.ticket_types.base_change_ticket.uuid", spec=['uuid4'])  # noqa
        UUIDMock.uuid4.return_value = "UUID-token"

        expected_ticket_data = {
            "Title": "Sol·licitud canvi de email oficina virtual",
            "Queue": "Oficina Virtual::Canvi email contractes::Rebut",
            "State": ChangeEmailTicket.TICKET_STATE,
            "Type": ChangeEmailTicket.TICKET_TYPE,
            "Priority": ChangeEmailTicket.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_message = "-"
        expected_article_data = {
            "Subject": "Sol·licitud canvi de email oficina virtual",
            "Body": expected_message
        }

        fields_dict = {
            "new_value": "newemail@email.coop",
            "scope": "some",
            "selected_subscriptions": ["1254"],
        }

        ChangeEmailTicket(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        expected_df_calls = [
            mocker.call('ProcessManagementProcessID', 'Process-b4243866849bc5c0c292f18100fad9d3'),  # noqa
            mocker.call('ProcessManagementActivityID', 'Activity-8cc76400ded45e7b408f6d3e0267a2c6'),  # noqa
            mocker.call('IDOV', UUIDMock.uuid4.return_value),
            mocker.call('refOdooContract', '1254'),
            mocker.call('nouEmail', 'newemail@email.coop'),
            mocker.call('flagContracts', True)
        ]
        for call in DynamicFieldMock.mock_calls:
            assert call in expected_df_calls

        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[
                mocker.ANY for c in expected_df_calls
            ]
        )

    def test_create_scope_all(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.ticket_types.base_ticket.OTRSClient",
            return_value=mocker.Mock()
        )
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())  # noqa
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())  # noqa
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())  # noqa
        mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())  # noqa
        UUIDMock = mocker.patch("somconnexio.providers.ticket_types.base_change_ticket.uuid", spec=['uuid4'])  # noqa
        UUIDMock.uuid4.return_value = "UUID-token"

        expected_ticket_data = {
            "Title": "Sol·licitud canvi de email oficina virtual",
            "Queue": "Oficina Virtual::Canvi email contractes::Rebut",
            "State": ChangeEmailTicket.TICKET_STATE,
            "Type": ChangeEmailTicket.TICKET_TYPE,
            "Priority": ChangeEmailTicket.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_message = "-"
        expected_article_data = {
            "Subject": "Sol·licitud canvi de email oficina virtual",
            "Body": expected_message
        }

        fields_dict = {
            "new_value": "newemail@email.coop",
            "scope": "all",
            "selected_subscriptions": ["111", "222"],
        }

        ChangeEmailTicket(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        expected_df_calls = [
            mocker.call('ProcessManagementProcessID', 'Process-b4243866849bc5c0c292f18100fad9d3'),  # noqa
            mocker.call('ProcessManagementActivityID', 'Activity-8cc76400ded45e7b408f6d3e0267a2c6'),  # noqa
            mocker.call('IDOV', UUIDMock.uuid4.return_value),
            mocker.call('refOdooContract', "111;222"),
            mocker.call("nouEmail", fields_dict["new_value"]),
            mocker.call('flagContracts', True)
        ]
        for call in DynamicFieldMock.mock_calls:
            assert call in expected_df_calls

        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[
                mocker.ANY for c in expected_df_calls
            ]
        )
