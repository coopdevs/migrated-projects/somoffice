import pytest

from pyotrs.lib import DynamicField

from somconnexio.providers.ticket_types.change_tariff_adsl_ticket import ChangeTariffTicketADSL


class TestCaseChangeADSLTariffTicket:
    def test_create(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.OTRSClient", return_value=mocker.Mock())
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())
        KeycloakRemoteUserBackendMock = mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())
        translationMock = mocker.patch("somconnexio.providers.ticket_types.change_tariff_ticket.translation", return_value=mocker.Mock())
        translationMock.get_language.return_value = "ca"

        expected_ticket_data = {
            "Title": "OV Sol·licitud CT A SF/F SF",
            "Queue": "Serveis de banda ampla::Provisió Fibra",
            "State": ChangeTariffTicketADSL.TICKET_STATE,
            "Type": ChangeTariffTicketADSL.TICKET_TYPE,
            "Priority": ChangeTariffTicketADSL.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_article_data = {
            "Subject": "OV Sol·licitud CT A SF/F SF",
            "Body": "-"
        }

        fields_dict = {
            "ref_contract": "1256",
            "landline_number": "969999999",
            "service_address": "Carrer",
            "service_city": "Ciutat",
            "service_zip": "ZIPCODE",
            "service_state": "State",
            "name": "Name",
            "sur_name": "Surname",
            "last_name": "Lastname",
            "complete_name": "CompleteName",
            "vat_number": "VAT",
            "iban": "IBAN",
            "customer_email": "customerEmail",
            "service_state_code": "stateCode",
            "customer_phone": "969999992"
        }

        ChangeTariffTicketADSL(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        calls = [
                mocker.call('ProcessManagementProcessID', 'Process-75757fe8d1526118bf0a723fde97b217'),
                mocker.call('ProcessManagementActivityID', 'Activity-4c41ac0caaa3601dff977822b7b83299'),
                mocker.call('refOdooContract', '1256'),
                mocker.call('telefonFixVell', '969999999'),
                mocker.call('direccioServei', 'Carrer'),
                mocker.call('poblacioServei', 'Ciutat'),
                mocker.call('CPservei', 'ZIPCODE'),
                mocker.call('provinciaServei', 'State'),
                mocker.call('productBA', 'SE_SC_REC_BA_F_100_SF'),
                mocker.call('serveiPrevi', 'ADSL'),
                mocker.call('nomSoci', 'Name'),
                mocker.call('titular', 'Name'),
                mocker.call('cognom1', 'Surname Lastname'),
                mocker.call('cognom1Titular', 'Surname Lastname'),
                mocker.call('nomCognomSociAntic', 'CompleteName'),
                mocker.call('NIFNIESoci', 'VAT'),
                mocker.call('NIFNIEtitular', 'VAT'),
                mocker.call('IBAN', 'IBAN'),
                mocker.call('telefonFixAntic', '969999999'),
                mocker.call('IDContracte', '-'),
                mocker.call('correuElectronic', 'customerEmail'),
                mocker.call('direccioServeiAntic', 'Carrer - ZIPCODE Ciutat State'),
                mocker.call('codiProvinciaServei', 'stateCode'),
                mocker.call('telefonContacte', '969999992')
        ]
        DynamicFieldMock.assert_has_calls(calls)
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[mocker.ANY for call in calls]
        )
