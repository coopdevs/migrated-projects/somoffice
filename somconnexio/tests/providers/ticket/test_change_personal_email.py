from somconnexio.providers.ticket_types.change_personal_email_ticket import \
    ChangePersonalEmailTicket


class TestCaseChangePersonalEmail:
    def test_create(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.ticket_types.base_ticket.OTRSClient",
            return_value=mocker.Mock()
        )
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())  # noqa
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())  # noqa
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())  # noqa
        mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())  # noqa
        UUIDMock = mocker.patch("somconnexio.providers.ticket_types.change_personal_email_ticket.uuid", spec=['uuid4'])  # noqa
        UUIDMock.uuid4.return_value = "UUID-token"

        expected_ticket_data = {
            "Title": "Canvi d'email dades personals OV",
            "Queue": "Oficina Virtual::Canvi email::Rebut",
            "State": ChangePersonalEmailTicket.TICKET_STATE,
            "Type": ChangePersonalEmailTicket.TICKET_TYPE,
            "Priority": ChangePersonalEmailTicket.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_article_data = {
            "Subject": "Canvi d'email dades personals OV",
            "Body": "-",
        }

        fields_dict = {
            "email": "fakeemail@email.coop",
        }

        ChangePersonalEmailTicket(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)

        expected_df_calls = [
            mocker.call('ProcessManagementProcessID', 'Process-b4243866849bc5c0c292f18100fad9d3'),  # noqa
            mocker.call('ProcessManagementActivityID', 'Activity-8cc76400ded45e7b408f6d3e0267a2c6'),  # noqa
            mocker.call('flagPartner', True),
            mocker.call("nouEmail", fields_dict["email"]),
            mocker.call('IDOV', UUIDMock.uuid4.return_value),
        ]
        for call in DynamicFieldMock.mock_calls:
            assert call in expected_df_calls

        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[mocker.ANY for call in expected_df_calls]
        )
