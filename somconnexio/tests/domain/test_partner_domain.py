import pytest
from somconnexio.domain.partner import get_partner_resource


class TestCasePartnerDomain:
    def test_get_partner_resource(self, mocker):
        customer_code = '123'
        expected_customer = object

        PartnerMock = mocker.patch(
            'somconnexio.domain.partner.Partner',
            spec_set=['get']
        )
        PartnerMock.get.return_value = expected_customer

        customer = get_partner_resource(customer_code)

        assert customer == expected_customer
        PartnerMock.get.assert_called_once_with(customer_code)

