import json
from pyopencell.client import Client
from pyopencell.resources.subscription import Subscription
from pyopencell.resources.customer_account import CustomerAccount
from pyopencell.resources.subscription_list import SubscriptionList
from somconnexio.domain.customer import user_accounts_for
from somoffice.core.secure_resources import build_secure_id_generator
from somconnexio.presenters.subscriptions import Subscriptions

def monkey_patch_opencell():
    original_send = Client._send_request

    def patched_send(self, *args, **kwargs):
        res = original_send(self, *args, **kwargs)

        log_output = json.dumps({
            "type": "opencell-debug",
            "elapsed": res.elapsed.total_seconds(),
            "request": dict(kwargs)
        })

        print(log_output)

        return res

    Client._send_request = patched_send



monkey_patch_opencell()

class SubscriptionsProvider:
    def __init__(self, options):
        self.options = options
        self.secure_id_generator = build_secure_id_generator(
            user_id=options["remote_user_id"], resource_type="subscription"
        )

    def get_resource(self, resource_id):
        subscription = Subscription.get(resource_id).subscription
        one_shot_services = Client().get(
            "/billing/subscription/listOneshotChargeOthers",
            subscriptionCode=resource_id,
        )
        all_one_shot_services = Client().get(
            "/billing/subscription/listOneshotChargeOthers"
        )

        subscription.services["oneShotServices"] = one_shot_services[
            "oneshotChargeInstances"
        ]

        self._preload_customer_accounts_for([subscription])

        presenter = Subscriptions(
            {
                "secure_id_generator": self.secure_id_generator,
                "customer_account_by_id": self.customer_account_by_id,
                "language": self.options["language"],
                "all_one_shot_services": all_one_shot_services["oneshotCharges"],
            }
        )

        return presenter.get_subscription(subscription)

    def get_resources(self):
        customer_code = self.options["remote_user_id"]
        subscriptions = []

        for user_account in user_accounts_for(customer_code):
            subscriptions += SubscriptionList.get(
                query="userAccount.code:{}|status:ACTIVE".format(user_account["code"])
            ).subscriptions

        self._preload_customer_accounts_for(subscriptions)

        presenter = Subscriptions(
            {
                "secure_id_generator": self.secure_id_generator,
                "customer_account_by_id": self.customer_account_by_id,
                "language": self.options["language"],
                "all_one_shot_services": None,
            }
        )

        return presenter.get_subscriptions(subscriptions)

    def _preload_customer_accounts_for(self, subscriptions):
        """
        To avoid doing too many requests to opencell API, we fetch all the
        deduped customer account ids. Otherwise, when listing subscriptions, we
        would be making one request to customer account endpoint per
        subscription.
        """
        customer_account_ids = set()

        for subscription in subscriptions:
            # userAccount and customerAccount ids are the same because of SC conventions
            customer_account_ids.add(subscription.userAccount)

        self.customer_account_by_id = {}

        for id in customer_account_ids:
            customer_account_response = CustomerAccount.get(id)
            self.customer_account_by_id[id] = customer_account_response.customerAccount
