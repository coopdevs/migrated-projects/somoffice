from django.utils import translation
from pyotrs.lib import DynamicField

from somconnexio.providers.ticket_types.base_customer_ticket import BaseCustomerTicket


class ChangeTariffTicket(BaseCustomerTicket):
    def get_search_args(self):
        return {
            "dynamic_fields": [
                DynamicField("ProcessManagementProcessID", self._get_process_id()),
                DynamicField("ProcessManagementActivityID", self._get_activity_id()),
            ],
            "Queues": [self._get_queue()],
            "States": ["new"],
        }

    def _get_activity_id(self):
        return "Activity-7117b19116339f88dc43767cd477f2be"

    def _get_process_id(self):
        return "Process-f91240baa6e0146aecc70a9c97d6f84f"

    def _get_subject(self):
        return "Sol·licitud Canvi de tarifa oficina virtual"

    def _get_queue(self):
        return "Oficina Virtual::Canvi Tarifa mòbil::Rebut"

    def _get_dynamic_fields(self):
        return {
            "renovaCanviTarifa": len(self.override_ticket_ids) > 0,
            "liniaMobil": self.fields["phone_number"],
            "productMobil": self.fields["new_product_code"],
            "tarifaAntiga": self.fields["current_product_code"],
            "dataExecucioCanviTarifa": self.fields["effective_date"],
            "correuElectronic": self.fields["subscription_email"],
            "idioma": translation.get_language()+"_ES",
        }
