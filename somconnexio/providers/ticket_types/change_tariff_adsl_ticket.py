from somconnexio.providers.ticket_types.base_change_tariff_ba_ticket import (
    BaseChangeTariffTicketBA,
)
from somconnexio.domain.helpers import format_full_address, get_complete_last_name


class ChangeTariffTicketADSL(BaseChangeTariffTicketBA):
    def _get_process_id(self):
        return "Process-75757fe8d1526118bf0a723fde97b217"

    def _get_activity_id(self):
        return "Activity-4c41ac0caaa3601dff977822b7b83299"

    def _get_subject(self):
        return "OV Sol·licitud CT A SF/F SF"

    def _get_queue(self):
        return "Serveis de banda ampla::Provisió Fibra"

    def _get_specfic_dynamic_fields(self):
        complete_last_name = get_complete_last_name(
            self.fields["sur_name"], self.fields["last_name"]
        )
        return {
            "serveiPrevi": "ADSL",
            "nomSoci": self.fields["name"],
            "titular": self.fields["name"],
            "cognom1": complete_last_name,
            "cognom1Titular": complete_last_name,
            "nomCognomSociAntic": self.fields["complete_name"],
            "NIFNIESoci": self.fields["vat_number"],
            "NIFNIEtitular": self.fields["vat_number"],
            "IBAN": self.fields["iban"],
            "telefonFixAntic": self.fields["landline_number"],
            "IDContracte": "-",
            "correuElectronic": self.fields["customer_email"],
            "direccioServeiAntic": format_full_address(
                self.fields["service_address"],
                self.fields["service_zip"],
                self.fields["service_city"],
                self.fields["service_state"],
            ),
            "codiProvinciaServei": self.fields["service_state_code"],
            "telefonContacte": self.fields["customer_phone"],
        }
