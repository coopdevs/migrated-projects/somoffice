from django.utils import translation

from somconnexio.providers.ticket_types.base_customer_ticket import BaseCustomerTicket


class AddDataTicket(BaseCustomerTicket):
    def _get_activity_id(self):
        return "Activity-d621f2e21d7583c0dffc7569cd3c9f59"

    def _get_process_id(self):
        return "Process-7f03577ba04a39dbc9e0fe0d2c6144df"

    def _get_subject(self):
        return "Sol·licitud abonament addicional oficina virtual"

    def _get_queue(self):
        return "Oficina Virtual::Dades addicionals::Rebut Dades"

    def _get_dynamic_fields(self):
        return {
            "liniaMobil": self.fields["phone_number"],
            "productAbonamentDadesAddicionals": self.fields["new_product_code"],
            "correuElectronic": self.fields["subscription_email"],
            "idioma": translation.get_language()+"_ES",
        }
