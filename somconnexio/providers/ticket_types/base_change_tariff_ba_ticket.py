from pyotrs.lib import DynamicField

from somconnexio.providers.ticket_types.base_customer_ticket import BaseCustomerTicket


class BaseChangeTariffTicketBA(BaseCustomerTicket):
    def get_search_args(self):
        return {
            "dynamic_fields": [
                DynamicField("ProcessManagementProcessID", self._get_process_id()),
                DynamicField("ProcessManagementActivityID", self._get_activity_id()),
            ],
            "Queues": [self._get_queue()],
            "States": ["new"],
        }

    def _get_specfic_dynamic_fields(self):
        raise NotImplementedError(
            "Tickets to change tariff BA must implement _get_specfic_dynamic_fields"
        )

    def _get_dynamic_fields(self):
        result = {
            **{
                "refOdooContract": self.fields["ref_contract"],
                "telefonFixVell": self.fields["landline_number"],
                "direccioServei": self.fields["service_address"],
                "poblacioServei": self.fields["service_city"],
                "CPservei": self.fields["service_zip"],
                "provinciaServei": self.fields["service_state"],
                "productBA": "SE_SC_REC_BA_F_100_SF",
            },
            **self._get_specfic_dynamic_fields(),
        }
        return result
