from pyotrs.lib import DynamicField

from somconnexio.providers.ticket_types.base_change_ticket import \
    BaseChangeTicket


class ChangeToConfirmTicket(BaseChangeTicket):
    QUEUES_TO_APPLIE_CHANGE = [
        "Oficina Virtual::Canvi IBAN contractes::Rebut",
        "Oficina Virtual::Canvi email contractes::Rebut",
        "Oficina Virtual::Canvi email::Rebut",
    ]

    def __init__(self):
        super().__init__(user=None, fields_dict=None)

    def _confirmation_dynamic_fields(self):
        return [DynamicField("enviatOdooOV", True)]

    def confirm(self, ticket_number):
        ticket = self.get(ticket_number)
        if ticket.field_get("Queue") in self.QUEUES_TO_APPLIE_CHANGE:
            self.update(
                ticket.field_get("TicketID"),
                dynamic_fields=self._confirmation_dynamic_fields())
        else:
            raise Exception(
                """
Change confirmed for ticket in unexpected queue.
- Ticket ID: {ticket_id}
- Queue: {queue}
                """.format(
                    ticket_id=ticket.field_get("TicketID"),
                    queue=ticket.field_get("Queue"),
                ))
