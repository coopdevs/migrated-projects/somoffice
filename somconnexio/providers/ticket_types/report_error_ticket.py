import json
from textwrap import dedent

from somconnexio.providers.ticket_types.base_ticket import BaseTicket


class ReportErrorTicket(BaseTicket):
    TICKET_TYPE = "Incidencia"

    def _get_queue(self):
        return "Sistemes::Error formularis"

    def _get_subject(self):
        return "Error submit"

    def _get_customer_code(self):
        return "sistemes@somconnexio.coop"

    def _get_body(self):
        message = f"""

            Se ha producido un error al intentar realizar el submit en los formularios con los siguientes datos:

            {json.dumps(self.fields["request"], ensure_ascii=False, indent=4)}
        """
        if self.fields["user"]:
            message = message + f"""

            Con el usuario con ref: {self.user.profile.remoteId}
            """
        return dedent(message)

    def _get_dynamic_fields(self):
        return []

    def _get_process_management_dynamic_fields(self):
        return []
