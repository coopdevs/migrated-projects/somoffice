import json
import os
import uuid
# TODO: Remove the settings dependency
from django.conf import settings

from pyotrs import Ticket, Article
from pyotrs.lib import DynamicField

from otrs_somconnexio.client import OTRSClient


class BaseTicket:
    TICKET_STATE = "new"
    TICKET_PRIORITY = "3 normal"

    def __init__(self, user, fields_dict, override_ticket_ids=[]):
        self.user = user
        self.fields = fields_dict
        self.client = None
        self.override_ticket_ids = override_ticket_ids

    def _get_customer_code(self):
        raise NotImplementedError("Tickets must implement _get_customer_code")

    def _get_subject(self):
        raise NotImplementedError("Tickets must implement _get_subject")

    def _get_body(self):
        return "-"

    def _get_queue(self):
        raise NotImplementedError("Tickets must implement _get_queue")

    def _get_dynamic_fields(self):
        raise NotImplementedError("Tickets must implement _get_dynamic_fields")

    def _dict_to_dynamic_fields(self, d):
        return [DynamicField(key, d[key]) for key in d]

    def send_ticket(self):
        customer_code = self._get_customer_code()

        subject = self._get_subject()
        body = self._get_body()
        queue = self._get_queue()

        article = Article({"Subject": subject, "Body": body})
        new_ticket = Ticket({
            "Title": subject,
            "Queue": queue,
            "State": self.TICKET_STATE,
            "Type": self.TICKET_TYPE,
            "Priority": self.TICKET_PRIORITY,
            "CustomerUser": customer_code,
            "CustomerID": customer_code
        })

        dynamic_fields = self._dict_to_dynamic_fields(
            self._get_process_management_dynamic_fields()
        ) + self._dict_to_dynamic_fields(self._get_dynamic_fields())

        # TODO hack alert, ideally this process tickets should live in
        # otrs-somconnexio package. Meanwhile we need to access the raw
        # PyOTRS client to create tickets.
        self._client().client.ticket_create(
            new_ticket, article=article, dynamic_fields=dynamic_fields,
        )

        return True

    def store_ticket(self):
        ticket_path = os.path.join(settings.SOMOFFICE_TICKET_FALLBACK_PATH, str(uuid.uuid4()) + ".json")

        # ensure ticket fallback path exists
        if not os.path.exists(settings.SOMOFFICE_TICKET_FALLBACK_PATH):
            os.makedirs(settings.SOMOFFICE_TICKET_FALLBACK_PATH)

        with open(ticket_path, "w") as f:
            json.dump({
                "user_id": self.user.id,
                "fields": self.fields
            }, f)

    def create(self):
        self.send_ticket()

    def get(self, idov):
        """ Get a ticket searching by IDOV DynamicField. """
        ticket = self._client().client.ticket_search(
            dynamic_fields=[DynamicField("IDOV", search_patterns=idov)]
        )
        if ticket:
            return self._client().client.ticket_get_by_id(ticket_id=ticket[0])

    def update(self, ticket_id, article=None, dynamic_fields=None):
        self._client().client.ticket_update(
            ticket_id,
            article=article,
            dynamic_fields=dynamic_fields,
        )

    def _client(self):
        if self.client:
            return self.client

        self.client = OTRSClient()

        return self.client


