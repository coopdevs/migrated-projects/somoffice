from somconnexio.providers.ticket_types.base_customer_ticket import \
    BaseCustomerTicket


class BaseChangeEmailTicket(BaseCustomerTicket):
    def _get_activity_id(self):
        return "Activity-8cc76400ded45e7b408f6d3e0267a2c6"

    def _get_process_id(self):
        return "Process-b4243866849bc5c0c292f18100fad9d3"

    def _get_queue(self):
        return "Oficina Virtual::Canvi email::Rebut"
