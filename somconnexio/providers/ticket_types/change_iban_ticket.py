from somconnexio.providers.ticket_types.base_change_ticket import BaseChangeTicket


class ChangeIbanTicket(BaseChangeTicket):
    def _get_activity_id(self):
        return "Activity-621e28c842e6556964d4a6ce04286235"

    def _get_process_id(self):
        return "Process-76f712cb8936190743ace3a886b3d167"

    def _get_queue(self):
        return "Oficina Virtual::Canvi IBAN contractes::Rebut"

    def _attribute_to_change(self):
        return "IBAN"

    def _get_subject(self):
        return "Sol·licitud canvi de IBAN oficina virtual"

    def _get_attribute_dynamic_field_name(self):
        return "nouIBAN"
