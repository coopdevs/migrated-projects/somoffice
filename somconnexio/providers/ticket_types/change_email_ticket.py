from somconnexio.providers.ticket_types.base_change_ticket import \
    BaseChangeTicket
from somconnexio.providers.ticket_types.base_change_email_ticket import \
    BaseChangeEmailTicket


class ChangeEmailTicket(BaseChangeTicket, BaseChangeEmailTicket):
    def _get_queue(self):
        return "Oficina Virtual::Canvi email contractes::Rebut"

    def _attribute_to_change(self):
        return "email"

    def _get_subject(self):
        return "Sol·licitud canvi de email oficina virtual"

    def _get_attribute_dynamic_field_name(self):
        return "nouEmail"

    def _get_dynamic_fields(self):
        result = super()._get_dynamic_fields()

        result.update({
            "flagContracts": True
        })

        return result
