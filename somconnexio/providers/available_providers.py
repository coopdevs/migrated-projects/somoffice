from odoo_somconnexio_python_client.resources.provider import Provider as AvailableProvider
from somconnexio.serializers.available_providers import AvailableProviderSerializer


# Note: here providers means service providers from somconnexio
class AvailableProviders:
    def __init__(self, options):
        self.options = options

    def get_resources(self):
        # TODO: retrieve the language variants
        if self.options["category"] == 'mobile':
            available_providers = AvailableProvider.mobile_list()
        else:
            available_providers = AvailableProvider.broadband_list()

        serializer = AvailableProviderSerializer(available_providers, many=True)

        return serializer.data
