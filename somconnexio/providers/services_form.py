from somconnexio.serializers.services_form import ServicesFormSerializer
from somconnexio.presenters.subscription_request import SubscriptionRequestPresenter
from somconnexio.presenters.crm_lead import CRMLeadPresenter
from odoo_somconnexio_python_client.resources.subscription_request import SubscriptionRequest
from odoo_somconnexio_python_client.resources.crm_lead import CRMLead


class ServicesFormProvider:
    def __init__(self, options):
        self.options = options

    def create(self, form, user=None):

        data = self.validate_data(form)

        if user:
            partner_id = user.profile.remoteId
            subscription_request_id = None
        else:
            partner_id = None
            subscription_request_id = self.create_subscription(data).id

        if not data.get("lines"):
            return None

        # Prepare CRMLead data
        crm_leads_data = CRMLeadPresenter(
            data,
            partner_id,
            subscription_request_id
        ).get_crm_lead()

        for crm_lead_data in crm_leads_data:
            # Create CRMLead
            CRMLead.create(**crm_lead_data)

        return None

    def validate_data(self, form):
        """Validate the ServiceForm data with the ServiceFormSerializer.
        Raise an exeption if an error is find.
        """
        serializer = ServicesFormSerializer(data=form)
        if not serializer.is_valid():
            # TODO: CustomException -> Create a ticket in OTRS with the request data
            raise Exception(serializer.errors)
        return serializer.data

    def create_subscription(self, data):
        # Prepare SubscriptionRequest data
        subscription_request_data = SubscriptionRequestPresenter(data).get_subscription_request()  # noqa
        # Create SubscriptionRequest
        return SubscriptionRequest.create(**subscription_request_data)
