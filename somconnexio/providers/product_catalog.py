from odoo_somconnexio_python_client.resources.product_catalog import ProductCatalog
from somconnexio.serializers.product_catalog import ProductCatalogSerializer


class ProductCatalogProvider:
    def __init__(self, options):
        self.options = options

    def get_resources(self):
        pricelists = ProductCatalog.search(
            code=self.options.get("code"), 
            category=self.options.get("category"),
            lang=self.options.get("language")
        )
        serializer = ProductCatalogSerializer(pricelists, many=True)

        return serializer.data
