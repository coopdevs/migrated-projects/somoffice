from django.urls import path
from . import views

urlpatterns = [
    # api/admin/ path is protected with a BA
    path("admin/import_user/", views.import_user),
    path("admin/user/", views.get_user),
    path("admin/change_user_email", views.change_user_email),

    path("confirm_change/", views.confirm_change),
]
