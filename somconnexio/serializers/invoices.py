from urllib.parse import urljoin
from django.urls import reverse
from django.conf import settings

from somoffice.views.invoices import download


class InvoicesSerializer:
    def __init__(self, options):
        self.secure_id_generator = options["secure_id_generator"]

    def serialize(self, invoices):
        return [
            self._invoice_to_json(invoice)
            for invoice in invoices
            if self._filter_invoice(invoice)
        ]

    def _filter_invoice(self, invoice):
        return len(invoice.invoiceNumber or "") > 0

    def _invoice_to_json(self, invoice):
        secure_id = self.secure_id_generator(invoice.invoiceId)

        return {
            "id": secure_id,
            "number": invoice.invoiceNumber,
            "date": invoice.invoiceDate,
            "total_amount": invoice.amountWithTax,
            "tax_amount": invoice.amountTax,
            "base_amount": invoice.amountWithoutTax,
            "download_url": self._get_download_url(secure_id, invoice),
        }

    def _get_invoice_filename(self, invoice):
        if len(invoice.pdfFilename or "") > 0:
            return invoice.pdfFilename

        return "%s-%s.pdf" % (invoice.invoiceDate, invoice.invoiceNumber)

    def _get_download_url(self, secure_id, invoice):
        return urljoin(
            settings.DEFAULT_DOMAIN,
            reverse(
                download,
                kwargs={
                    "secure_id": secure_id,
                    "filename": self._get_invoice_filename(invoice),
                },
            ),
        )
