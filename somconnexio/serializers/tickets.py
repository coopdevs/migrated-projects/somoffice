from rest_framework import serializers

class TicketSerializer(serializers.Serializer):
    id = serializers.CharField()
    meta = serializers.JSONField()
