from rest_framework import serializers


class ProductSerializer(serializers.Serializer):
    code = serializers.CharField()
    name = serializers.CharField()
    price = serializers.DecimalField(max_digits=6, decimal_places=2)
    category = serializers.CharField()
    minutes = serializers.IntegerField()
    data = serializers.IntegerField()
    bandwidth = serializers.IntegerField()
    available_for = serializers.ListField(child=serializers.CharField())


class ProductCatalogSerializer(serializers.Serializer):
    code = serializers.CharField()
    products = ProductSerializer(many=True)
