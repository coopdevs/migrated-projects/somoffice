from rest_framework import serializers


class UserSerializer(serializers.Serializer):
    vat = serializers.CharField()
    email = serializers.CharField()
    lang = serializers.CharField()
