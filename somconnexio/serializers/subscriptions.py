from rest_framework import serializers


class ServiceSerializer(serializers.Serializer):
    type = serializers.CharField()
    code = serializers.CharField()
    date = serializers.IntegerField()
    description = serializers.CharField()
    action = serializers.CharField()
    status = serializers.CharField()


class AddressSerializer(serializers.Serializer):
    zip_code = serializers.CharField()
    address = serializers.CharField()
    subdivision = serializers.CharField()
    city = serializers.CharField()


class SubscriptionsSerializer(serializers.Serializer):
    id = serializers.CharField()
    active_product_code = serializers.CharField()
    code = serializers.CharField()
    description = serializers.CharField()
    email = serializers.CharField()
    iban = serializers.CharField()
    complete_name = serializers.CharField()
    name = serializers.CharField()
    sur_name = serializers.CharField()
    last_name = serializers.CharField()
    subscription_type = serializers.CharField()
    vat_number = serializers.CharField()
    services = ServiceSerializer(many=True)
    address = AddressSerializer()
    available_operations = serializers.ListField(
        child=serializers.CharField()
    )
