class SubscriptionRequestPresenter:
    """
    Presenter to create the SubscriptionRequest object in Odoo.
    Create a SR dict with an ServicesForm object.
    """
    def __init__(self, services_form):
        self.services_form = services_form

    def get_subscription_request(self):
        # TODO: Aclarar que me pasan si es un usuario logged
        if "partner_id" in self.services_form:
            return None

        data = {
            "type": self._type(),
            "email": self.services_form.get("email"),
            # "phone": self.services_form.get("phone"),
            "address": {
                "street": self.services_form.get("address").get("street"),
                "zip_code": self.services_form.get("address").get("zip_code"),
                "city": self.services_form.get("address").get("city"),
                "state": self.services_form.get("address").get("state"),
                "country": self.services_form.get("address").get("country"),
            },
            "lang": self._lang(),
            "iban": (
                self.services_form.get("iban") or \
                self.services_form.get("service_iban")
            ),
            "vat": self.services_form.get("vat"),
            "payment_type": self.services_form.get("payment_type"),
            "nationality": self.services_form.get("nationality"),
            "discovery_channel_id": int(self.services_form.get("discovery_channel_id")),
            "gender": self.services_form.get("gender"),
            "phone": self.services_form.get("phone"),
            "birthdate": self.services_form.get("birthdate"),
        }
        if self.services_form.get("is_company"):
            data.update(self._company_data())
        else:
            data.update(self._personal_data())

        if self.services_form.get("sponsor_vat"):
            data["sponsor_vat"] = self.services_form.get("sponsor_vat", "")

        if self.services_form.get("coop_agreement_code"):
            data["coop_agreement"] = self.services_form.get("coop_agreement_code")

        # Clean keys with None value from the dict
        data = {x: y for x, y in data.items() if y is not None}
        return data

    def _company_data(self):
        return {
            "is_company": True,
            "name": self.services_form.get("name"),
            "company_name": self.services_form.get("tradename"),
        }

    def _personal_data(self):
        return {
            "is_company": False,
            "name": " ".join(
                [
                    self.services_form.get("name"),
                    self.services_form.get("surname"),
                    self.services_form.get("lastname"),
                ]
            ),
            "firstname": self.services_form.get("name"),
            "lastname": " ".join(
                [
                    self.services_form.get("surname"),
                    self.services_form.get("lastname")
                ]
            ),
        }

    def _is_company(self):
        return self.services_form.get("is_company")

    def _type(self):
        if self.services_form.get("sponsor_vat"):
            return "sponsorship"
        elif self.services_form.get("coop_agreement_code"):
            return "sponsorship_coop_agreement"
        return "new"

    def _lang(self):
        if self.services_form.get("lang") == "es":
            return "es_ES"
        return "ca_ES"
