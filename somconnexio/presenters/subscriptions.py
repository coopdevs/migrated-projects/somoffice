import re
from somconnexio.domain.helpers import format_customer_name, build_custom_fields_dict, get_surName, get_lastName


class Subscriptions:
    def __init__(self, options):
        self.options = options
        self.secure_id_generator = options["secure_id_generator"]
        self.customer_account_by_id = options["customer_account_by_id"]
        self.all_one_shot_services = options["all_one_shot_services"]

    def get_subscription(self, subscription):
        return self.subscription_to_json(subscription)

    def get_subscriptions(self, subscriptions):
        return self._filter_after_serialization(
            [
                self.subscription_to_json(sub)
                for sub in subscriptions
                if self._is_item_visible(sub)
            ]
        )

    def subscription_to_json(self, subscription):
        secure_id = self.secure_id_generator(subscription.code)
        customer_account = self.customer_account_by_id[subscription.userAccount]
        services = self._services_from(subscription, include_one_shot=False)
        active_product_code = self._get_active_product_code_from_services(services)

        return {
            "id": secure_id,
            "code": subscription.code,
            "description": subscription.description,
            "vat_number": customer_account.vatNo,
            "email": customer_account.contactInformation["email"],
            "complete_name": format_customer_name(customer_account.name),
            "name": customer_account.name["firstName"],
            "sur_name": get_surName(customer_account.name["lastName"]),
            "last_name": get_lastName(customer_account.name["lastName"]),
            "iban": self._get_preferred_method_of_payment(customer_account).get('bankCoordinates', {}).get('iban'),
            "subscription_type": self._get_subscription_type(subscription),
            "services": services,
            "active_product_code": active_product_code,
            "address": self._get_subscription_address(subscription),
            "available_operations": self._get_available_operations(subscription,active_product_code),
        }

    def _get_available_operations(self, subscription,active_product_code):
        available_operations = []

        if subscription.offerTemplate == 'OF_SC_TEMPLATE_BA':
            if active_product_code == 'SE_SC_REC_BA_F_100' or active_product_code == 'SE_SC_REC_BA_ADSL_SF':
                available_operations.append("ChangeTariffOutLandline")

        return available_operations

    def _get_active_product_code_from_services(self, services):
        active_service = next(
            (service for service in services if service["status"] == "ACTIVE"), None
        )

        if not active_service:
            return None

        return active_service["code"]

    def _get_subscription_address(self, subscription):
        custom_fields = subscription.customFields["customField"]
        fields_dict = build_custom_fields_dict(custom_fields)

        if "CF_OF_SC_SUB_SERVICE_ADDRESS" not in fields_dict:
            return None

        return {
            "zip_code": fields_dict["CF_OF_SC_SUB_SERVICE_CP"],
            "address": fields_dict["CF_OF_SC_SUB_SERVICE_ADDRESS"],
            "subdivision": fields_dict["CF_OF_SC_SUB_SERVICE_SUBDIVISION"],
            "city": fields_dict["CF_OF_SC_SUB_SERVICE_CITY"],
        }

    def _translate_locale_code(self):
        locale = self.options["language"]

        if locale == "es":
            return "ES"
        if locale == "ca":
            return "CAT"

    def _translate_locale_code_language_descriptions(self):
        locale = self.options["language"]

        if locale == "es":
            return "ESP"
        if locale == "ca":
            return "CAT"

    def _services_from(self, subscription, include_one_shot=False):
        sub_services = [
            self._service_to_json(service)
            for service in subscription.services["serviceInstance"]
        ]

        if include_one_shot:
            one_shot_services = [
                self._one_shot_service_to_json(service)
                for service in subscription.services["oneShotServices"]
            ]
        else:
            one_shot_services = []

        combined_services = sub_services + one_shot_services

        return [
            service for service in combined_services if self._is_valid_service(service)
        ]

    def _one_shot_service_to_json(self, one_shot_service):
        matching_service = next(
            (
                service
                for service in self.all_one_shot_services
                if service["code"] == one_shot_service["code"]
            ),
            None,
        )

        if not matching_service:
            return None

        custom_field_locale = self._translate_locale_code()
        language_description_locale = (
            self._translate_locale_code_language_descriptions()
        )

        localized_description = [
            description["description"]
            for description in matching_service["languageDescriptions"]
            if description["languageCode"] == language_description_locale
        ]

        custom_fields_dict = build_custom_fields_dict(
            matching_service["customFields"]["customField"]
        )
        action_field = "CF_SC_ACTION_" + custom_field_locale

        if action_field in custom_fields_dict:
            action = custom_fields_dict[action_field]
        else:
            action = None

        return {
            "type": "one_shot_service",
            "date": one_shot_service["operationDate"],
            "description": localized_description,
            "action": action,
            "status": one_shot_service["status"],
        }

    def _service_to_json(self, service):
        custom_fields = service["customFields"]["inheritedCustomField"]
        locale = self._translate_locale_code()

        custom_fields_dict = build_custom_fields_dict(custom_fields)

        description_field = "CF_LANGUAGE_" + locale
        action_field = "CF_SC_ACTION_" + locale

        if description_field in custom_fields_dict:
            description = custom_fields_dict[description_field]
        else:
            description = service["description"]

        if action_field in custom_fields_dict:
            action = custom_fields_dict[action_field]
        else:
            action = None

        return {
            "type": "service",
            "code": service["code"],
            "date": service["subscriptionDate"],
            "description": description,
            "action": action,
            "status": service["status"],
        }

    def _get_subscription_type(self, subscription):
        offer = subscription.offerTemplate

        if offer == 'OF_SC_TEMPLATE_MOB':
            return "mobile"
        elif offer == 'OF_SC_TEMPLATE_BA':
            return "broadband"
        else:
            return "unknown"

    def _is_item_visible(self, subscription):
        custom_fields = subscription.customFields["customField"]
        fields_dict = build_custom_fields_dict(custom_fields)

        if "CF_SUB_SC_HIDE" in fields_dict:
            return fields_dict["CF_SUB_SC_HIDE"] == "NO"
        else:
            return True

    def _is_valid_service(self, service):
        if service is None:
            return False

        return service["status"] == "ACTIVE"

    def _filter_after_serialization(self, subscriptions):
        return [sub for sub in subscriptions if sub["subscription_type"] != "unknown"]

    # gets preferred method of payment, fallsback to first one
    def _get_preferred_method_of_payment(self, customer_account):
        methods_of_payment = customer_account.methodOfPayment

        return next(
            (method for method in methods_of_payment if method["preferred"]),
            methods_of_payment[0],
        )
