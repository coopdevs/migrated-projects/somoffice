class User:
    def __init__(self, vat, keycloak_user, somoffice_user):
        self.vat = vat
        self.email = keycloak_user['email']
        self.lang = somoffice_user.profile.preferredLocale
