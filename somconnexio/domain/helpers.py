import re

def _fix_formatting(name):
    """
    Fixes weird formatting of "LOPEZ, PEREZ," into "Lopez Perez"
    """

    name = re.sub(r",$", "", name)
    name = re.sub(r", *", ", ", name)

    return " ".join([part.lower().capitalize() for part in name.split(", ")])


def format_customer_name(customer_name):
    first_name = customer_name["firstName"]
    last_name = customer_name["lastName"]

    return "%s %s" % (first_name, _fix_formatting(last_name))

def format_full_address(service_address,service_zip,service_city,service_state):
    return "%s - %s %s %s" % (service_address, service_zip, service_city, service_state)

def get_surName(name):
    return _fix_formatting(name).split(" ")[0]


def get_lastName(name):
    if len(_fix_formatting(name).split(" ")) > 1:
        return _fix_formatting(name).split(" ")[1]
    else:
        return ""


def get_complete_last_name(sur_name, last_name ):
        return "%s %s" % (sur_name,last_name)

def build_custom_fields_dict(custom_fields):
    """
    Builds a custom field dictionary to ease lookup by name.

    Takes an array of custom fields, and builds a dictionary whose keys are
    custom field names, and values are custom field values.

    Basically turns this

        [
              {
                "code": "FOO",
                "description": "Foo",
                "fieldType": "STRING",
                "stringValue": "foo",
                ...
              },
              {
                "code": "BAR",
                "description": "Commission rate (%)",
                "fieldType": "DOUBLE",
                "doubleValue": 10,
                ...
              }
        ]


    Into this:

        {
            "FOO": "foo",
            "BAR": 10
        }

    """

    def _get_value_key(field):
        """
        Custom field value can come in different keys ( "stringValue",
        "doubleValue", etc ...)

        This helper function returns the first key that ends with "Value"
        """
        return [key for key in field.keys() if key.endswith("Value")][0]

    fields_dict = {}

    for field in custom_fields:
        valueKey = _get_value_key(field)

        if valueKey is None:
            continue

        fields_dict[field["code"]] = field[valueKey]

    return fields_dict
