from pyopencell.resources.customer import Customer
from pyopencell.exceptions import PyOpenCellAPIException


def get_customer_resource(customer_code):
    """
    Safely tries to get customer resource from Opencell API

    Returns None if user cannot be found
    """
    try:
        return Customer.get(customer_code).customer
    except PyOpenCellAPIException as e:
        # TODO: maybe we should do something else
        return None


def customer_accounts_for(customer_code):
    customer_resource = get_customer_resource(customer_code)

    if customer_resource is None:
        return

    for customer_account in customer_resource.customerAccounts["customerAccount"]:
        yield customer_account


def billing_accounts_for(customer_code):
    for customer_account in customer_accounts_for(customer_code):
        for billing_account in customer_account["billingAccounts"]["billingAccount"]:
            yield billing_account


def user_accounts_for(customer_code):
    for billing_account in billing_accounts_for(customer_code):
        for user_account in billing_account["userAccounts"]["userAccount"]:
            yield user_account
