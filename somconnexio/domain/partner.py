from odoo_somconnexio_python_client.resources.partner import Partner


def get_partner_resource(customer_code):
    """
    Safely tries to get customer resource from Odoo API

    Returns None if user cannot be found
    """
    try:
        return Partner.get(customer_code)
    except Exception as e:
        return None
