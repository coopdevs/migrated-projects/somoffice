# Proceso:

# 1. Recuperamos usuarios de OV
# 2. Buscamos el usuario en Odoo
# 3. Buscamos el usuario en Keycloak
# 3.1 Si no encontramos el usuario, lo buscamos por VAT sin el ES.
# 4. Actualizamos los datos de Keycloak con los de Odoo
# 5. Actualizamos los datos de OV con los de Odoo

# Recorrer usuarios de SO y por cada usuario revisar si:
#   - Username en Keycloak (VAT) y VAT en Odoo son iguales
#   - Email en Keycloak y Email en Odoo son iguales
from keycloak import KeycloakAdmin

from odoo_somconnexio_python_client.resources.partner import Partner

from django.db import IntegrityError

from django.conf import settings

from somoffice.models import User, Profile
import re
import time

tic = time.perf_counter()

somoffice_users_updated = 0
somoffice_users_deleted = 0
somoffice_users_to_review = 0
keycloak_users_updated = 0
keycloak_users_created = 0
keycloak_users_to_review = 0


keycloak_admin = KeycloakAdmin(
        server_url=settings.KEYCLOAK_SERVER_URL + "/auth/",
        username=settings.KEYCLOAK_ADMIN_USER,
        password=settings.KEYCLOAK_ADMIN_PASSWORD,
        realm_name=settings.KEYCLOAK_REALM,
        verify=True,
        auto_refresh_token=['get', 'put', 'post', 'delete'],
)


def check_odoo_partner (odoo_partner, ref):

    def _is_odoo_partner_provider(odoo_partner):
        return not odoo_partner.coop_candidate and \
                not odoo_partner.member and \
                not odoo_partner.sponsor_id and \
                not odoo_partner.coop_agreement_code

    if not odoo_partner:
        print(f"ERROR: User {ref} without Odoo partner.")
    elif _is_odoo_partner_provider(odoo_partner):
        print(f"WARNING: User {ref} is a Odoo provider")
    elif not odoo_partner.vat:
        print(f"ERROR: User {ref} without Odoo partner VAT.")
    elif not odoo_partner.email:
        print(f"ERROR: User {ref} without Odoo partner email.")
    else:
        return True
    return False

def create_user_in_keycloak(keycloak_admin, partner):
    print(f"Creating {partner.ref} in Keycloak")
    keycloak_admin.create_user(
        {
            "email": partner.email,
            "username": partner.vat.lower(),
            "enabled": True,
            "attributes": {"customerCode": partner.ref},
        }
    )
    global keycloak_users_created
    keycloak_users_created+=1
    print(f"Created {partner.ref} in Keycloak")


def update_user_in_keycloak(keycloak_admin, keycloak_user, partner):
    print(f"Updating {partner.ref} in Keycloak")
    keycloak_admin.update_user(
        keycloak_user["id"], {
            **keycloak_user,
            **{
                "email": partner.email,
                "username": partner.vat.lower(),
            }
        }
    )
    global keycloak_users_updated
    keycloak_users_updated+=1
    print(f"Updated {partner.ref} in Keycloak")


def _remove_OV_user(user):
    user.delete()
    global somoffice_users_deleted
    somoffice_users_deleted+=1
    print(f"User {user} deleted")


def _check_customer_code(customer_code):
    return customer_code.isdigit()


# SOMOFFICE #
# Recuperem tots els usuaris de la DB de SomOffice
# users = User.objects.all().order_by("id")
users = User.objects.filter(
    username__in=[
        "es48603222j",
        "es41581508s",
        "es46769933p",
        "es39388800n",
        "es74011636m",
        "es46238638j",
        "es46238638-j",
        "es47959942c",
    ]
)
print("*************** INIT ***************")

for user in users:
    print("\n----\n")
    customer_code = None
    vat_number = None
    try:
        customer_code = user.profile.remoteId
        vat_number = user.username
        print(f"Working with user {customer_code} CustomerCode.")
    except Exception:
        print(f"ERROR: User {user} without profile or username .")
        _remove_OV_user(user)
        continue

    if(not _check_customer_code(customer_code)):
        print(f"ERROR: user {customer_code} CustomerCode is WRONG")
        _remove_OV_user(user)
        continue

    # ODOO #
    # Fem una crida a l'API REST d'Odoo per recuperar el partner d'Odoo
    try:
        odoo_partner = Partner.get(customer_code)
    except Exception:
        print(f"ERROR: getting the user {user} from Odoo")
        _remove_OV_user(user)
        continue

    if not check_odoo_partner(odoo_partner, user):
        _remove_OV_user(user)
        continue

    # Revisamos si el username (VAT) en OV no es el de Odoo:
    # En ese caso buscamos mas usuarios en OV con el mismo customer_code:
    # Si existen y alguno tiene el VAT como en Odoo borramos el usuario actual (sobre el que estamos trabajando).
    # Si no lo actualizamos con el VAT de Odoo.
    if odoo_partner.vat.lower() != vat_number:
        users_in_somoffice = User.objects.filter(
            id__in=Profile.objects.filter(remoteId=customer_code).values_list(
                "user_id", flat=True
            )
        )
        if odoo_partner.vat.lower() in [
            value
            for user_vat in users_in_somoffice.values("username")
            for value in user_vat.values()
        ]:
            print("WARNING: Another record with this CustomerCode Exist in Somoffice that corresponds to the partner, deleting the present from somOffice_user & somoffice_profile")  # noqa
            _remove_OV_user(user)
            continue
    print("Updating username in somoffice from odoo")  # noqa
    user.username = odoo_partner.vat.lower()
    try:
        user.save(force_update=True)
    except IntegrityError:
        print("WARNING: user.username is duplicated in somoffice, checking ....")
        repeated_user = User.objects.get(username=odoo_partner.vat.lower())
        repeated_user_remoteId = Profile.objects.get(user_id=repeated_user.id).remoteId
        if(repeated_user_remoteId == customer_code):
            print("WARNING: Another record with this CustomerCode Exist in Somoffice that corresponds to the partner, deleting the present from somOffice_user & somoffice_profile")  # noqa
            _remove_OV_user(user)
            continue
        else:
            print(f"WARNING :: Review { repeated_user_remoteId }/{ repeated_user } & {customer_code}/{ user }")
            somoffice_users_to_review += 1
            continue

    print(f"Updated user { customer_code } in SomOffice")
    somoffice_users_updated+=1
    # Fem una crida a l'API REST de Keycloak per recuperar l'usuari a Keycloak
    vat_only_number = re.sub(r"\D", "", odoo_partner.vat)
    keycloak_users = (keycloak_admin.get_users({"username": vat_only_number}) or [])  # noqa

    if not keycloak_users:
        print(f"ERROR: User with vat like {odoo_partner.vat} doesn't exists in Keycloak.")  # noqa
        create_user_in_keycloak(keycloak_admin, odoo_partner)
        continue

    if len(keycloak_users) == 1:
        keycloak_user = keycloak_users[0]
        update_user_in_keycloak(keycloak_admin, keycloak_user, odoo_partner)
        continue

    # Multiple Customers in keycloak with username as vat_only_number:
    for keycloak_user in keycloak_users:
        is_to_update = False
        k_user_name = keycloak_user["username"]
        if not keycloak_user.get("attributes"):
            print(f"ERROR: Customer in keycloak {k_user_name} does not have CustomerCode")
            keycloak_users_to_review+=1
            continue
        k_customer_code = keycloak_user["attributes"]["customerCode"][0]


        # keycloak_user has the same remoteID as the present odooRef
        if odoo_partner.ref == k_customer_code:
            is_to_update = True
        else:
            try:
                print(f"Searching in Odoo partner for keycloak_customer_code")
                k_odoo_partner = Partner.get(k_customer_code)
            except Exception:
                print(f"ERROR: getting the keycloak_user {k_customer_code} from Odoo")
                keycloak_users_to_review+=1
                continue

            if not check_odoo_partner(k_odoo_partner, k_customer_code):
                print(f"ERROR: Customer in keycloak {k_customer_code} isn't in Odoo")
                keycloak_users_to_review+=1
                continue

            is_to_update = True

        if is_to_update:
            print(f"Updating keycloak_user {k_customer_code} - {k_user_name} from Odoo")
            try:
                update_user_in_keycloak(keycloak_admin, keycloak_user, odoo_partner)
            except Exception:
                keycloak_users_to_review+=1
                print(f"ERROR: updating user {k_user_name} in Keycloak")
        else:
            keycloak_users_to_review+=1
            print(f"WARNING: Review {k_user_name} not found in Odoo")


toc = time.perf_counter()

print("**************** END ***************")
print(f"*************** Total Time Executing :: {toc-tic:0.4f} seconds *************************")
print(f"*************** keycloak_users_created :: {keycloak_users_created} *********************")
print(f"*************** keycloak_users_updated :: {keycloak_users_updated} *********************")
print(f"*************** keycloak_users_to_review :: {keycloak_users_to_review} *****************")
print(f"*************** somoffice_users_updated :: {somoffice_users_updated} *******************")
print(f"*************** somoffice_users_deleted :: {somoffice_users_deleted} *******************")
print(f"*************** somoffice_users_to_review :: {somoffice_users_to_review} ***************")

