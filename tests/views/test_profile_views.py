import pytest
from somoffice.models import User, Profile, EmailConfirmationToken
from django.utils import timezone
from rest_framework.test import APIClient
from datetime import timedelta


class TestProfile:
    def test_profile_without_session(self):
        client = APIClient()

        response = client.get("/api/profile/")

        assert response.status_code == 403

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_profile_with_session(self):
        client, _user = self._authenticate(username="55642302N")

        response = client.get("/api/profile/")

        assert response.status_code == 200

    def test_change_email_without_session(self):
        client = APIClient()

        response = client.post(
            "/api/profile/request_email_change/", {"new_email": "joseluis@mailinator.com"}
        )

        assert response.status_code == 403

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_change_email_with_session(self):
        client, _user = self._authenticate()

        response = client.post(
            "/api/profile/request_email_change/", {"new_email": "joseluis@mailinator.com"}
        )

        assert response.status_code == 200

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_confirm_email_change_with_invalid_token(self):
        client, _user = self._authenticate()

        response = client.post(
            "/api/profile/confirm_email_change/",
            {"token": "hehehe", "new_email": "newjoseluis@mailinator.com"},
        )

        assert response.status_code == 422

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_confirm_email_change_with_valid_token(self):
        user = self._create_user()
        client = APIClient()

        EmailConfirmationToken.objects.create(
            user=user,
            token="thetoken",
            expiresAt=timezone.now() + timedelta(days=1),
            email="joseluis@mailinator.com",
        )

        response = client.post(
            "/api/profile/confirm_email_change/",
            {"token": "thetoken", "new_email": "newjoseluis@mailinator.com"},
        )

        assert response.status_code == 200

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_confirm_email_change_with_expired_token(self):
        user = self._create_user()
        client = APIClient()

        EmailConfirmationToken.objects.create(
            user=user,
            token="thetoken",
            expiresAt=timezone.now() + timedelta(days=-1),
            email="joseluis@mailinator.com",
        )

        response = client.post(
            "/api/profile/confirm_email_change/",
            {"token": "thetoken", "new_email": "newjoseluis@mailinator.com"},
        )

        assert response.status_code == 422

    def test_change_password_without_session(self):
        client = APIClient()

        response = client.post(
            "/api/profile/change_password/",
            {"current_password": "Kilahph5", "new_password": "iphuS0ie"},
        )

        assert response.status_code == 403

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_change_password_with_session_and_valid_password(self):
        client, user = self._authenticate()

        user.set_password("Kilahph5")

        response = client.post(
            "/api/profile/change_password/",
            {"current_password": "Kilahph5", "new_password": "iphuS0ie"},
        )

        assert response.status_code == 200

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_change_password_with_session_and_invalid_password(self):
        client, user = self._authenticate()

        user.set_password("Kilahph5")

        response = client.post(
            "/api/profile/change_password/",
            {"current_password": "idunnolol", "new_password": "iphuS0ie"},
        )

        assert response.status_code == 422

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_profile_with_member_role(self):
        client, _user = self._authenticate(username="55642302N")

        response = client.get("/api/profile/")

        assert response.status_code == 200
        assert "role" in response.data
        assert response.data["role"] == "member"

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_profile_with_candidate_as_member_role(self):
        client, _user = self._authenticate(username="16651490L")

        response = client.get("/api/profile/")

        assert response.status_code == 200
        assert "role" in response.data
        assert response.data["role"] == "member"

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_profile_with_coop_agreement_role(self):
        client, _user = self._authenticate(username="16651490L")

        response = client.get("/api/profile/")

        assert response.status_code == 200
        assert "role" in response.data
        assert response.data["role"] == "coop_agreement"

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_profile_with_sponsored_role(self):
        client, _user = self._authenticate(username="07650352T")

        response = client.get("/api/profile/")

        assert response.status_code == 200
        assert "role" in response.data
        assert response.data["role"] == "sponsored"

    def _authenticate(self, username="55771114X"):
        user = self._create_user(username)

        client = APIClient()

        client.force_authenticate(user=user)

        return client, user

    def _create_user(self, username="55771114X"):
        user = User.objects.create(username=username)

        Profile.objects.create(
            remoteBackend="opencell", remoteId="1017", preferredLocale="ca", user=user
        )

        return user
