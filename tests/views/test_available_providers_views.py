import pytest
from rest_framework import status
from rest_framework.test import APITestCase


class TestAvailableProviders(APITestCase):
    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_available_providers_without_category(self):

        response = self.client.get("/api/providers/")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_available_providers_with_category(self):

        response = self.client.get("/api/providers/?category=mobile")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for channel in response.json():
            assert "id" in channel
            assert "name" in channel
