import json
import pytest

from rest_framework.test import APITestCase


class TestServicesForm(APITestCase):
    def setUp(self):
        self.form = {
            "is_company": False,
            "vat": "2123",
            "sponsor_vat": None,
            "name": "Name",
            "tradename": None,
            "surname": "Surname",
            "lastname": "Lastname",
            "email": "email@email.com",
            "birthdate": "1992-05-03",
            "gender": "male",
            "lang": "ca",
            "nationality": "ES",
            "discovery_channel_id": 2,
            "payment_type": "single",
            "iban": "ESXXXXX",
            "service_iban": None,
            "address": {
                "street": "Carrer",
                "zip_code": "08001",
                "city": "Barcelona",
                "state": "C",
                "country": "ES",
            },
            "agrees_to_voluntary_contribution": False,
            "iban": "ESXXXXX",
        }

    @pytest.mark.vcr
    def test_submit_person(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_MOBILE_T_150_0",
                    "type": "mobile",
                    "mobile_isp_info": {
                        "type": "new",
                        "delivery_address": {
                              "street": "Carrer",
                              "zip_code": "08001",
                              "city": "Barnalona",
                              "state": "CA",
                              "country": "ES"
                         },
                    }
                }
            ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 200

    @pytest.mark.vcr
    def test_submit_org(self):
        form = {
            "is_company": True,
            "vat": "2123",
            "sponsor_vat": None,
            "name": "SOS Org",
            "tradename": "SOS Org",
            "email": "email@email.com",
            "lang": "ca",
            "nationality": "ES",
            "discovery_channel_id": 2,
            "payment_type": "single",
            "iban": "ESXXXXX",
            "service_iban": None,
            "address": {
                "street": "Carrer",
                "zip_code": "08001",
                "city": "Barcelona",
                "state": "C",
                "country": "ES",
            },
            "agrees_to_voluntary_contribution": False,
            "iban": "ESXXXXX",
            "lines": [
                {
                    "product_code": "SE_SC_REC_MOBILE_T_150_0",
                    "type": "mobile",
                    "mobile_isp_info": {
                        "type": "new",
                        "delivery_address": {
                              "street": "Carrer",
                              "zip_code": "08001",
                              "city": "Barnalona",
                              "state": "CA",
                              "country": "ES"
                         },
                    }
                }
            ]
        }
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 200

    @pytest.mark.vcr
    def test_submit_mobile_new_missing_delivery_adress(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_MOBILE_T_150_0",
                    "type": "mobile",
                    "mobile_isp_info": {
                        "type": "new",
                    }
                }
            ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 400

    @pytest.mark.vcr
    def test_submit_mobile_portability_missing_icc_donor(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_MOBILE_T_150_0",
                    "type": "mobile",
                    "mobile_isp_info": {
                            "type": "portability",
                            "previous_provider": 3,
                            "previous_contract_type": "prepaid",
                            "previous_owner_vat": "1234",
                            "previous_owner_name": "Name",
                            "previous_owner_surname": "Surname",
                            "previous_owner_lastname": "Lastname"
                    }
                }
            ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 400

    @pytest.mark.vcr
    def test_submit_logged_in_type_portability_missing_previous_data(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_MOBILE_T_150_0",
                    "type": "mobile",
                    "mobile_isp_info": {
                            "type": "portability",
                            "icc_donor": "231231",
                            "phone_number": "699111333",
                            "previous_provider": 3,
                    }
                }
            ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 400

    @pytest.mark.vcr
    def test_submit_broadband_new_ok(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_BA_ADSL_100",
                    "type": "broadband",
                    "broadband_isp_info": {
                        "type": "new",
                        "service_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            },
                        "delivery_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            }
                        }
                    }
                ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 200

    @pytest.mark.vcr
    def test_submit_new_broadband_service_without_delivery_address(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_BA_ADSL_100",
                    "type": "broadband",
                    "broadband_isp_info": {
                        "type": "new",
                        "service_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            }
                        }
                    }
                ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 400

    @pytest.mark.vcr
    def test_submit_portability_broadband_service(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_BA_ADSL_100",
                    "type": "broadband",
                    "broadband_isp_info": {
                        "type": "portability",
                        "delivery_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            },
                        "phone_number": "699111333",
                        "service_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            },
                        "previous_provider": 3,
                        "previous_service": "adsl"
                    }
                }
            ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 200

    @pytest.mark.vcr
    def test_submit_portability_broadband_service_without_phone_number(self):
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_BA_ADSL_100",
                    "type": "broadband",
                    "broadband_isp_info": {
                        "type": "portability",
                        "delivery_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            },
                        "service_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            },
                        "previous_provider": 3,
                        "previous_service": "adsl"
                    }
                }
            ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 400

    @pytest.mark.vcr
    def test_submit_portability_broadband_service_without_previous_service(self):  # noqa
        form = self.form.copy()
        form.update({
            "lines": [
                {
                    "product_code": "SE_SC_REC_BA_ADSL_100",
                    "type": "broadband",
                    "broadband_isp_info": {
                        "type": "portability",
                        "phone_number": "699111333",
                        "delivery_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            },
                        "service_address": {
                            "street": "Carrer",
                            "zip_code": "08001",
                            "city": "Barnalona",
                            "state": "CA",
                            "country": "ES"
                            },
                        "previous_provider": 3
                    }
                }
            ]
        })
        url = "/api/services-form/submit/"
        data = json.dumps(form)

        response = self.client.post(url, data, content_type='application/json')

        assert response.status_code == 400

# TODO: Rebuild tests in the logged in branch
#
#    @pytest.mark.vcr
#    def test_submit_logged_in_successful(self):
#        form = {
#            "partner_id": 1234,
#            "iban": "ESXXXXX",
#            "lines": [
#                {
#                    "product_code": "SE_SC_REC_MOBILE_T_150_0",
#                    "type": "mobile",
#                    "mobile_isp_info": {
#                        "delivery_address": {
#                              "street": "Carrer",
#                              "zip_code": "08001",
#                              "city": "Barnalona",
#                              "state": "CA",
#                              "country": "ES"
#                         },
#
#                    }
#                }
#            ]
#        }
#        url = "/api/services-form/submit/"
#        data = json.dumps(form)
#
#        response = self.client.post(url, data, content_type='application/json')
#
#        assert response.status_code == 200
#
#    @pytest.mark.vcr
#    def test_submit_logged_in_unsuccessful(self):
#        form = {
#                "partner_id": 0,
#                "iban": "ESXXXXX",
#                "lines": [
#                    {
#                        "product_code": "SE_SC_REC_MOBILE_T_150_0",
#                        "type": "mobile",
#                        "mobile_isp_info": {
#                            "delivery_address": {
#                                  "street": "Carrer",
#                                  "zip_code": "08001",
#                                  "city": "Barnalona",
#                                  "state": "CA",
#                                  "country": "ES"
#                             },
#                        }
#                    }
#                ]
#        }
#        url = "/api/services-form/submit/"
#        data = json.dumps(form)
#
#        response = self.client.post(url, data, content_type='application/json')
#
#        assert response.status_code == 400
