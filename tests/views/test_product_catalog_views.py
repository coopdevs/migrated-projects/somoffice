import decimal
import pytest
from rest_framework import status
from rest_framework.test import APITestCase


class TestTariffs(APITestCase):
    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_tariffs_without_code(self):
        response = self.client.get("/api/product-catalog/")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_tariffs_with_nonexistant_code(self):
        response = self.client.get("/api/product-catalog/?code=XXXX")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 0)

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_tariffs_with_existant_code(self):
        response = self.client.get("/api/product-catalog/?code=21IVA")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        products = response.json()[0].get('products')
        self.assertEqual(len(products), 35)

        product_names = []
        product_codes = []
        product_prices = []
        product_available_for = []

        for product in products:
            product_names.append(product["name"])
            product_codes.append(product["code"])
            product_prices.append(decimal.Decimal(product["price"]))
            product_available_for.append(product["available_for"])

        self.assertIn(
            "ADSL 1000 min a fix",
            product_names
        )
        self.assertIn(
            ["member"],
            product_available_for
        )
        self.assertIn(
            "SE_SC_REC_BA_F_100",
            product_codes
        )
        self.assertIn(
            36.00,
            product_prices
        )

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_tariffs_with_category_filter(self):
        response = self.client.get("/api/product-catalog/?code=21IVA&categ=mobile")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        products = response.json()[0].get('products')
        self.assertEqual(len(products), 29)

        product_codes = []
        for product in products:
            product_codes.append(product["code"])
            self.assertEqual(product["category"], "mobile")

        self.assertIn(
            "SE_SC_REC_MOBILE_T_0_0",
            product_codes
        )
        self.assertNotIn(
            "SE_SC_REC_BA_F_100",
            product_codes
        )
