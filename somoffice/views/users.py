from rest_framework import status
from rest_framework.response import Response
from somoffice.core import load_resource_provider
from rest_framework.decorators import api_view


@api_view(("GET",))
def search_by_vat(request):
    vat = request.query_params.get("vat")
    options = {}
    if not vat:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        provider = load_resource_provider("users")(options)

        if not provider.vat_exists(vat):
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_200_OK)

@api_view(("GET",))
def check_user(request):
    vat = request.query_params.get("vat")
    sponsor_code = request.query_params.get("code")
    options = {}
    provider = load_resource_provider("users")(options)
    allowed, message = provider.check_user(vat, sponsor_code)
    return Response(data={"allowed": allowed, "message": message}, status=status.HTTP_200_OK)

