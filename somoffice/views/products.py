from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from somoffice.core.http import with_secure_token
from somoffice.core import load_resource_provider


class Products(APIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    @with_secure_token(secured_param="subscription_id", uses_django_rest=True)
    def get(self, request, secure_token):
        subscription_id = secure_token["resource_id"]

        options = {
            "remote_user_id": request.user.profile.remoteId,
            "language": request.user.profile.preferredLocale,
        }

        provider = load_resource_provider("products")(options)

        products = provider.get_resources(
            subscription_id=subscription_id,
            product_type=request.query_params["product_type"],
        )

        return Response(products)
