from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from somoffice.core import load_resource_provider
from rest_framework.authentication import SessionAuthentication


class ServicesForm(APIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = []

    def post(self, request):
        options = {
            "remote_user_id": "",
            "language": "",
        }

        user = None
        if hasattr(request.user, "profile"):
            user = request.user
            options["remote_user_id"] = request.user.profile.remoteId
            options["language"] = request.user.profile.preferredLocale

        try:
            provider = load_resource_provider("services_form")(options)
            provider.create(
                form=request.data,
                user=user,
            )
            return Response({"msg": "ok"})
        except Exception as e:
            print("Error creating submition form")
            print(repr(e))
            return self._report_error(user, request.data, options)

    def _report_error(self, user, data, options):
        print("Reporting submit error")
        provider = load_resource_provider("tickets")(options)
        meta = [
            {
                "key": "ticket_type",
                "value": "report_error",
            },
            {
                "key": "request",
                "value": data,
            },
            {
                "key": "user",
                "value": user,
            },
        ]
        print("Error context: {}".format(meta))
        try:
            provider.create(
                user=user,
                meta=meta,
            )
        except Exception as e:
            print("Error creating report ticket.")
            raise e
        return Response({"msg": "error"}, status=status.HTTP_400_BAD_REQUEST)
