from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse, JsonResponse
from somoffice.core.http import requires_authentication
from django.contrib.auth.forms import PasswordResetForm

from django.core.mail import EmailMultiAlternatives
from django.dispatch import receiver
from django.template import loader
from django.urls import reverse
from django.conf import settings
from django.utils.module_loading import import_string

from django_rest_passwordreset.signals import reset_password_token_created
from django_rest_passwordreset.views import ResetPasswordRequestToken
from rest_framework import serializers

from somoffice.core import load_resource_provider

from urllib.parse import urljoin
import json


@ensure_csrf_cookie
def set_csrf_cookie(request):
    """
    This will be `/api/set-csrf-cookie/` on `urls.py`
    """

    return JsonResponse({"details": "CSRF cookie set"})


@require_POST
def login_view(request):
    """
    This will be `/api/login/` on `urls.py`
    """
    data = json.loads(request.body)
    username = data.get("username")
    password = data.get("password")

    if username is None or password is None:
        return JsonResponse(
            {"errors": {"__all__": "Please enter both username and password"}},
            status=400,
        )

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        return JsonResponse({"detail": "Success"})
    return JsonResponse({"detail": "Invalid credentials"}, status=400,)


@require_POST
@requires_authentication
def logout_view(request):
    """
    This will be `/api/login/` on `urls.py`
    """

    logout(request)

    return JsonResponse({"detail": "Bye bye!"})


@requires_authentication
def info(request):
    return JsonResponse({
        "session_cookie_age_in_seconds": settings.SESSION_COOKIE_AGE
    });


# TODO #django-rest-passwordreset
#
# here we are subclassing ResetPasswordRequestToken view so we can reset
# password by username instead of email.
#
# One annoying detail is that the payload that this view receives has this shape:
#
# { "email": "<the-username>" }
#
# We can remove this once this get merged
# https://github.com/anexia-it/django-rest-passwordreset/pull/93 and all "TODO
# #django-rest-passwordreset" grep matches :B
class UsernameSerializer(serializers.Serializer):
    email = serializers.CharField()


class ResetPasswordRequestTokenFromUsername(ResetPasswordRequestToken):
    serializer_class = UsernameSerializer


reset_password_request_token_from_username = (
    ResetPasswordRequestTokenFromUsername.as_view()
)


@receiver(reset_password_token_created)
def password_reset_token_created(
    sender, instance, reset_password_token, *args, **kwargs
):
    """
    Handles password reset tokens
    When a token is created, an e-mail needs to be sent to the user
    :param sender: View Class that sent the signal
    :param instance: View Instance that sent the signal
    :param reset_password_token: Token Model Object
    :param args:
    :param kwargs:
    :return:
    """

    profile = reset_password_token.user.profile
    locale = profile.preferredLocale
    username = reset_password_token.user.username

    AuthenticationBackend = import_string(
        settings.AUTHENTICATION_BACKENDS[0]
    )

    user = AuthenticationBackend().get_keycloak_user(username)

    if not user:
        raise Exception(
            "Error sending email. Cannot find keycloak user for %s." % username
        )

    email = user["email"]

    # send an e-mail to the user
    context = {
        "current_user": reset_password_token.user,
        "username": reset_password_token.user.username,
        "email": email,
        "reset_password_url": urljoin(
            settings.FRONTEND_BASE_URL, "/password-reset/" + reset_password_token.key
        ),
    }

    # TODO maybe use gettext
    subject_by_locale = {
        "ca": "Canvi de contrasenya per {title}",
        "es": "Cambio de contraseña para {title}",
    }

    # render email text
    email_plaintext_message = loader.render_to_string(
        "password_reset_email.%s.txt" % locale, context
    )

    msg = EmailMultiAlternatives(
        # title:
        subject_by_locale[locale].format(title="Somconnexio"),
        # message:
        email_plaintext_message,
        settings.EMAIL_FROM,
        # to:
        [email],
        bcc = ['info@somconnexio.coop'],
    )

    msg.send()
