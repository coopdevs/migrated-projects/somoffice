from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils import translation
from somoffice.core import load_resource_provider


class DiscoveryChannels(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request):
        # The active language gets set in the middleware custom method at somoffice/core/http.py
        active_lang = translation.get_language()
        options = {
            "language": active_lang,
        }

        provider = load_resource_provider("discovery_channels")(options)

        return Response(provider.get_resources())
