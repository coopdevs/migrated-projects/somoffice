from rest_framework import status
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from somoffice.core.http import with_secure_token
from somoffice.core import load_resource_provider
from somoffice.core.email_confirmation import get_email_confirmation_by_token, create_email_confirmation_token, check_email_confirmation, EmailConfirmationTokenNotFound, EmailConfirmationExpired, send_confirmation_email
from rest_framework.decorators import action
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

class EmailAddress:
    pass

class ProfileViewSet(ViewSet):
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    # GET /api/profile/ is list method, not ideal pero peor es morirse
    def list(self, request):
        provider = self._get_provider(request.user)

        profile = provider.get_resource()

        return Response(profile)

    @action(methods=["post"], detail=False)
    def request_email_change(self, request):
        email = request.data["new_email"]

        provider = self._get_provider(request.user)

        try:
            provider.request_email_change(email)
            return Response({"message": "ok"})
        except Exception as e:
            print(e)
            return Response(
                {"message": "unexpected error"}, status.HTTP_400_BAD_REQUEST
            )

    @action(methods=["post"], detail=False, authentication_classes=[], permission_classes=[])
    def confirm_email_change(self, request):
        try:
            token = request.data["token"]
            email_confirmation = get_email_confirmation_by_token(token)

            check_email_confirmation(email_confirmation)
            provider = self._get_provider(email_confirmation.user)
            result = provider.change_email(email_confirmation.email)

            if result:
                new_email = email_confirmation.email
                email_confirmation.delete()
                return Response({"new_email": new_email})
            else:
                return Response(
                    {"message": "unexpected error"}, status.HTTP_400_BAD_REQUEST
                )
        except (EmailConfirmationExpired, EmailConfirmationTokenNotFound) as e:
            return Response(
                {"message": "invalid token"}, status.HTTP_422_UNPROCESSABLE_ENTITY
            )


    @action(methods=["post"], detail=False)
    def change_password(self, request):
        current_password = request.data["current_password"]
        new_password = request.data["new_password"]

        if not request.user.check_password(current_password):
            return Response(
                {
                    "errors": [
                        _(
                            "Your old password was entered incorrectly. Please enter it again."
                        )
                    ]
                },
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

        try:
            validate_password(new_password)
            request.user.set_password(new_password)
            return Response({"message": "ok"})
        except ValidationError as errors:
            return Response(
                {"errors": errors}, status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )
        except Exception as e:
            print("Error ocurred while changing user password")
            print(e)
            return Response(
                {"message": "unexpected error"}, status=status.HTTP_400_BAD_REQUEST
            )

    def _get_provider(self, user):
        options = {
            "current_user": user,
            "remote_user_id": user.profile.remoteId,
        }

        return load_resource_provider("profile")(options)
