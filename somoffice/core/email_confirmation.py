import secrets
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils import timezone
from urllib.parse import urljoin

from somoffice.models import EmailConfirmationToken

class EmailConfirmationTokenNotFound(Exception):
    pass

class EmailConfirmationExpired(Exception):
    pass

def create_email_confirmation_token(user, email):
    token = secrets.token_urlsafe(64)[0:64]

    EmailConfirmationToken.objects.create(
        user=user,
        token=token,
        expiresAt=timezone.now() + settings.EMAIL_CONFIRMATION_PERIOD,
        email=email
    )

    return token


def send_confirmation_email(user, email, token):
    locale = user.profile.preferredLocale

    subject_by_locale = {
        "ca": "Confirmació de canvi de correu electrònic",
        "es": "Confirmación de cambio de e-mail"
    }

    context = {
        "confirmation_link": urljoin(
            settings.FRONTEND_BASE_URL,
            "/confirm-email/" + token
        )
    }

    # render email text
    email_plaintext_message = loader.render_to_string(
        "confirm_email_change.%s.txt" % locale, context
    )

    msg = EmailMultiAlternatives(
        # title:
        subject_by_locale[locale],
        # message:
        email_plaintext_message,
        settings.EMAIL_FROM,
        # to:
        [email],
    )

    msg.send()

def check_email_confirmation(email_confirmation):
    if timezone.now() > email_confirmation.expiresAt:
        raise EmailConfirmationExpired

    return True

def get_email_confirmation_by_token(token):
    try:
        return EmailConfirmationToken.objects.get(
            token=token,
        )
    except EmailConfirmationToken.DoesNotExist:
        raise EmailConfirmationTokenNotFound
