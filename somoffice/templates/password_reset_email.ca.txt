Hem rebut una petició per restablir la contrasenya del compte de Somconnexio associada amb {{email}}.
Pots canviar la contrasenya fent clic en aquest enllaç:

{{ reset_password_url }}

-
Somconnexio
