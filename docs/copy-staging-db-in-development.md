## Copy staging/production DB in development environment

```sh
# staging > dev

ssh <your-user>@somoffice.coopdevs.org
sudo su - somoffice
pg_dump somoffice > somoffice.db
exit
exit
ssh -A somoffice@local.somoffice.coop
scp <your-user>@somoffice.coopdevs.org:/home/somoffice/somoffice.db .
dropdb somoffice
createdb somoffice
psql somoffice < somoffice.db
```

```sh
# production > dev

ssh <your-user>@oficinavirtual.somconnexio.coop
sudo su - somoffice
pg_dump somoffice > somoffice.db
exit
exit
ssh -A somoffice@local.somoffice.coop
scp <your-user>@oficinavirtual.somconnexio.coop:/home/somoffice/somoffice.db .
dropdb somoffice
createdb somoffice
psql somoffice < somoffice.db
```
