import create from "zustand";
import { devtools } from "zustand/middleware";
import { findLastIndex, partition, uniqBy, compact } from "lodash";
import { v4 as uuidv4 } from "uuid";

const personalDataSteps = ["partner/personal-data", "partner/additional-data"];

const stampLineId = (line) => {
  if (line.__id) {
    return line;
  }

  return {
    __id: uuidv4(),
    ...line,
  };
};

const computeSteps = ({ lines, loggedIn, optingForRole }) => {
  const lineSteps = lines.flatMap((line) => [
    `line-${line.__id}/tariff`,
    `line-${line.__id}/additional-data`,
  ]);

  return [
    ...(loggedIn ? [] : personalDataSteps),
    ...lineSteps,
    ...compact([
      mustPayMonthlyBill({ lines }) && "payment/monthly-bill",
      mustPayMemberFee({ optingForRole, loggedIn }) && "payment/member-fee",
    ]),
  ];
};

// TODO add action
//
// - removeTariffAt
// - adding/removing tariffs should affect steps
//
// TODO add derived state
//
// - canAddMobileLine
// - canAddInternetLine
export const useStore = create(
  devtools((set, get) => ({
    sessionExpired: false,
    setSessionExpired: () => set(() => ({ sessionExpired: true })),
    currentIndex: 0,
    setCurrentIndex: (currentIndex) => set(() => ({ currentIndex })),

    formStepDataByKey: {},
    steps: [],
    rejectedLines: [],
    loggedIn: null,
    currentRole: null,
    optingForRole: null,
    coopAgreementCode: null,
    initialIntent: null,
    initializeSignupFlow: ({
      loggedIn,
      currentRole,
      optingForRole,
      coopAgreementCode,
      lines,
      tariffs,
      initialIntent,
    }) => {
      const findTariffByCode = (code) =>
        tariffs.find((tariff) => tariff.code === code);

      let [allowedLines, rejectedLines] = partition(lines, (line) => {
        if (!Boolean(line.code)) {
          return true;
        }

        return (findTariffByCode(line.code)?.available_for || []).includes(
          currentRole || optingForRole
        );
      });

      allowedLines = allowedLines.map(stampLineId);

      return set({
        currentRole,
        optingForRole,
        coopAgreementCode,
        loggedIn,
        lines: allowedLines,
        rejectedLines,
        initialIntent,
        steps: computeSteps({
          lines: allowedLines,
          loggedIn,
          optingForRole,
        }),
      });
    },
    setCurrentStep: (step) =>
      set((state) => ({ currentIndex: state.steps.indexOf(step) })),
    gotoNextStep: () =>
      set((state) => ({ currentIndex: state.currentIndex + 1 })),

    lines: [],
    setLines: (lines) => set(() => ({ lines: lines.map(stampLineId) })),

    availableAddresses: [],
    availablePaymentMethods: [],

    saveAddress: (address) =>
      set((state) => {
        if (!address?.street) {
          return {};
        }

        const newAddress = { ...address };

        if (!newAddress._id) {
          newAddress._id = uuidv4();
        }

        return {
          availableAddresses: uniqBy(
            [...state.availableAddresses, newAddress],
            "_id"
          ),
        };
      }),

    savePaymentMethod: (paymentMethod) =>
      set((state) => {
        return state;
      }),

    setAvailableAddresses: (availableAddresses) =>
      set(() => ({ availableAddresses })),

    setAvailablePaymentMethods: (availablePaymentMethods) =>
      set(() => ({ availablePaymentMethods })),

    setFormStepData: (key, data) =>
      set((state) => ({
        formStepDataByKey: {
          ...state.formStepDataByKey,
          [key]: data,
        },
      })),

    /**
     * Internet lines comes first, so we need to add it before the first mobile
     * line, are append at the end
     *
     */
    addInternetLine: () =>
      set((state) => {
        const lines = state.lines;
        const newLine = stampLineId({ type: "internet" });

        let result = {};

        const insertBeforeIndex = lines.findIndex(
          (line) => line.type === "mobile"
        );

        let nextVal;

        if (insertBeforeIndex === -1) {
          nextVal = [...lines, newLine];
        } else {
          nextVal = [...lines];
          nextVal.splice(insertBeforeIndex, 0, newLine);
        }

        result.lines = nextVal;

        result.steps = computeSteps({
          lines: nextVal,
          loggedIn: state.loggedIn,
          optingForRole: state.optingForRole,
        });

        return result;
      }),

    /**
     * Mobile lines are always added at the end
     */
    addMobileLine: () =>
      set((state) => {
        const newLine = stampLineId({ type: "mobile" });

        const result = {
          lines: [...state.lines, newLine],
        };

        result.steps = computeSteps({
          lines: result.lines,
          loggedIn: state.loggedIn,
          optingForRole: state.optingForRole,
        });

        return result;
      }),

    updateLineAt: (tariff, type, index) =>
      set((state) => {
        const nextVal = [...state.lines];
        const __id = state.lines[index].__id;

        nextVal.splice(index, 1, { __id, type, ...tariff });

        return { lines: nextVal };
      }),

    removeLine: (id) =>
      set((state) => {
        const nextVal = state.lines.filter(line => line.__id !== id);

        const targetLineIndex = state.lines.findIndex(
          (line) => line.__id === id
        );

        // Find next line to focus, the previous, or the next one if we are at the first;
        const nextLineToFocus = state.lines[targetLineIndex - 1] || state.lines[targetLineIndex + 1];

        const result = { lines: nextVal };

        result.steps = computeSteps({
          lines: result.lines,
          loggedIn: state.loggedIn,
          optingForRole: state.optingForRole,
        });

        // Find next step to focus after steps are recomputed
        const nextCurrentIndex = result.steps.findIndex(step => step.startsWith(`line-${nextLineToFocus.__id}`));

        result.currentIndex = nextCurrentIndex;

        return result;
      }),
  }))
);

const canAddLineOfType = (state) => (type) => {
  if (!state.initialIntent) {
    return false;
  }

  const wantedMobileInitially = state.initialIntent.lines.some(
    (line) => line.type === "mobile"
  );
  const wantedInternetInitially = state.initialIntent.lines.some(
    (line) => line.type === "internet"
  );

  if (type === "internet") {
    return (
      (state.optingForRole || state.currentRole) === "member" &&
      wantedInternetInitially &&
      state.lines.filter((line) => line.type === "internet").length < 2
    );
  } else if (type === "mobile") {
    return (
      wantedMobileInitially &&
      state.lines.filter((line) => line.type === "mobile").length < 4
    );
  } else {
    return false;
  }
};

const mustPayMemberFee = (state) =>
  !state.loggedIn && state.optingForRole === "member";
const mustPayMonthlyBill = (state) => (state.lines.length || []) > 0;

export const useDerivedState = () => {
  const state = useStore((state) => state);

  return {
    currentStep: state.steps[state.currentIndex],
    canAddLineOfType: canAddLineOfType(state),
    shouldAskForPersonalData: !state.loggedIn,
    mustPayMemberFee: mustPayMemberFee(state),
    mustPayMonthlyBill: mustPayMonthlyBill(state),
    getAddressById: (id) => state.availableAddresses.find((a) => a._id === id),
  };
};
