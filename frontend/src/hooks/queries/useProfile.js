import { getProfile } from "lib/api/profile";
import { useQuery } from "react-query";

export const useProfile = () => useQuery("profile", () => getProfile());

