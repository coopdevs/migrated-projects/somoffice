import React, { useState } from "react";
import { Box } from "@material-ui/core";
import { Container } from "components/layouts/Container";
import { Checkbox } from "components/Checkbox";
import { Link } from "components/Link";
import { Button } from "components/Button";
import { post } from "axios";
import { Form } from "components/Form";
import { Spinner } from "components/Spinner";
import { useDerivedState, useStore } from "hooks/useStore";
import { Tiles } from "components/layouts/Tiles";
import { LineStepGroup } from "./shared/StepGroups/LineStepGroup";
import { PersonalDataStepGroup } from "./shared/StepGroups/PersonalDataStepGroup";
import { PaymentStepGroup } from "./shared/StepGroups/PaymentStepGroup";
import { formToApi } from "./shared/formToApi";
import { Trans, useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { getLineGroupProps } from "./shared/getLineGroupProps";
import { useApplicationContext } from "hooks/useApplicationContext";

const waitFor = ms => new Promise(resolve => setTimeout(resolve, ms));

const Content = ({ tariffs }) => {
  const { currentUser } = useApplicationContext();
  const loggedIn = Boolean(currentUser);
  const lines = useStore(state => state.lines);
  const optingForRole = useStore(state => state.optingForRole);
  const { t } = useTranslation();
  const [hasAcceptedTerms, setHasAcceptedTerms] = useState(false);
  const history = useHistory();
  const { mustPayMonthlyBill, mustPayMemberFee } = useDerivedState();
  const state = useStore(state => state);
  const [isLoading, setIsLoading] = useState(null);

  const termsUrl = t("urls.terms");

  const gotoThanks = () => {
    history.push("thanks");
  };

  const onSubmit = async () => {
    const payload = formToApi(state, {
      loggedIn,
      mustPayMonthlyBill,
      mustPayMemberFee
    });

    try {
      setIsLoading(true);
      await post("/api/services-form/submit/", payload);
      await waitFor(250);

      gotoThanks();
    } catch (e) {
      console.error("Error submiting form!");
      console.error(e);
      gotoThanks();
    } finally {
      setIsLoading(false);
    }
  };

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <>
      <Tiles columns={1} spacing={1}>
        {!loggedIn && (
          <PersonalDataStepGroup optingForRole={optingForRole} confirmMode />
        )}
        {lines.map((line, index) => {
          return (
            <LineStepGroup
              confirmMode
              line={line}
              stepsBefore={2}
              tariffs={tariffs}
              index={index}
              isLast={false}
              {...getLineGroupProps(index, lines)}
            />
          );
        })}
        <PaymentStepGroup confirmMode />
        <Tiles columns="2" spacing={4}>
          <div />
          <Checkbox
            name="accept_terms"
            onChange={event => setHasAcceptedTerms(event.target.checked)}
            label={
              <Trans i18nKey="funnel.signup.confirm.accept_terms">
                <Link target="_blank" to={termsUrl} />
              </Trans>
            }
          />
          <div />
          <Button disabled={!hasAcceptedTerms} type="submit" onClick={onSubmit}>
            {t("common.finish")}
          </Button>
        </Tiles>
      </Tiles>
      <Box height="400px" />
    </>
  );
};

export const Confirm = ({ tariffs }) => (
  <Container variant="narrow">
    <Content tariffs={tariffs} />
  </Container>
);
