import React from "react";
import { Text } from "components/Text";
import { Card } from "components/Card";
import { Separator } from "components/Separator";
import { Box } from "@material-ui/core";
import { FormStepGroupContext } from "./FormStepGroupContext";

export const FormStepGroup = ({ title, children, index, confirmMode = false, topRightAction = null }) => {
  const shouldRenderSeparator = React.Children.count(children) > 1;

  return <FormStepGroupContext.Provider value={{confirmMode}}>
    <Card
      data-qa-selector="form-step-group"
      shadow={false}
      variant="noGutter"
      bgcolor="white"
      headerBgcolor={confirmMode ? "white" : undefined}
      header={
        <Box px={[3, 8]} display="flex" flexDirection="row" justifyContent="space-between" alignItems="center" width='100%'>
          <Text size="xs">
            {!confirmMode && <>{index}. &nbsp;&nbsp;</>}
            <Text semibold size="xs" uppercase letterSpacing='0.1rem'>
              {title}
            </Text>
          </Text>
          {topRightAction}
        </Box>
      }
    >
      {confirmMode && shouldRenderSeparator && <Separator />}
      {React.Children.map(children, (child, childIndex) => (
        <>
          {child}
          {childIndex < React.Children.count(children) - 1 && shouldRenderSeparator && <Separator />}
        </>
      ))}
    </Card>
  </FormStepGroupContext.Provider>
};
