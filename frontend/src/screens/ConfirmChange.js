import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";

import Box from "@material-ui/core/Box";

import { FullScreenCenteredLayout } from "components/layouts/FullScreenCenteredLayout";

import { Spinner } from "components/Spinner";
import { Link } from "components/Link";
import { Stack } from "components/layouts/Stack";
import { confirmChange } from "lib/api/profile";
import { useApplicationContext } from "hooks/useApplicationContext";

const Success = () => {
  const { t } = useTranslation();

  return (
    <Stack>
      <Alert severity="success">{t("confirm_change.complete")}</Alert>
      <Link to="/">{t("confirm_change.go_to_app")}</Link>
    </Stack>
  );
};

export function ConfirmChange() {
  const { token } = useParams();
  const { t } = useTranslation();
  //  const { updateCurrentUser } = useApplicationContext();

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [isSuccess, setIsSuccess] = useState(false);

  useEffect(() => {
    async function confirmToken() {
      try {
        setIsLoading(true);
        await confirmChange({ token });
        setIsLoading(false);
        setIsSuccess(true);
      } catch (e) {
        setError(t("confirm_change.invalid_or_expired_token"));
      } finally {
        setIsLoading(false);
      }
    }

    confirmToken();
  }, [token]);

  return (
    <FullScreenCenteredLayout>
      {isLoading && <Spinner />}
      {isSuccess && <Success />}
      {error && (
        <Box width="100%">
          <Alert severity="error">{error}</Alert>
        </Box>
      )}
    </FullScreenCenteredLayout>
  );
}
