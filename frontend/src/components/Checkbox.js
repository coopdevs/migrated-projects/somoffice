import {
  Checkbox as MaterialCheckbox,
  FormControlLabel
} from "@material-ui/core";
import { Field } from "react-final-form";
import { Text } from "components/Text";

const CheckedIcon = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="0.5"
      y="0.5"
      width="23"
      height="23"
      stroke="#3E3382"
      stroke-opacity="0.6"
    />
    <rect x="5" y="5" width="14" height="14" fill="#3E3382" />
  </svg>
);

const UncheckedIcon = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="0.5"
      y="0.5"
      width="23"
      height="23"
      stroke="#3E3382"
      stroke-opacity="0.6"
    />
  </svg>
);

export const Checkbox = ({ name, label, onChange, checked }) => {
  return (
    <FormControlLabel
      label={<Text>{label}</Text>}
      control={
        <MaterialCheckbox
          type="checkbox"
          icon={<UncheckedIcon />}
          checkedIcon={<CheckedIcon />}
          name={name}
          checked={checked}
          onChange={onChange}
        />
      }
    />
  );
};

Checkbox.FormField = ({ name, validate, initialValue, ...props }) => (
  <Field name={name} validate={validate} initialValue={initialValue} type="checkbox">
    {({ input, meta }) => (
      <Checkbox
        {...props}

        checked={input.checked}
        onChange={input.onChange}
      />
    )}
  </Field>
);
