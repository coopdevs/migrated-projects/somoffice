import React from "react";
import { Spinner } from "components/Spinner";
import { Tiles } from "components/layouts/Tiles";
import { Button } from "components/Button";
import { useQuery } from "react-query";
import { useTranslation } from "react-i18next";

const wrapQuery = query => async (...args) => {
  const result = await query(...args);

  if (result.data) {
    return result.data;
  }

  return result;
};

export const ResourceList = ({ query, queryKey, renderItem, renderHeader }) => {
  const { data, refetch, error, isLoading, isRefetching } = useQuery(
    queryKey,
    wrapQuery(query)
  );
  const { t } = useTranslation();

  if (isLoading || isRefetching) {
    return <Spinner />;
  }

  if (error) {
    return (
      <Tiles columns={1}>
        <div>{t("common.errors.request_failed")}</div>
        <Button onClick={refetch}>{t("common.retry")}</Button>
      </Tiles>
    );
  }

  return (
    <Tiles columns={1}>
      {renderHeader()}
      {data.map(renderItem)}
    </Tiles>
  );
};
