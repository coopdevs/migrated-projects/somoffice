import { getStateByValue } from "lib/helpers";
import { uniqWith, isEqual } from 'lodash'

export const mapLegacyAddress = address => {
  return {
    zip_code: address.zip_code,
    street: address.address,
    city: address.city,
    state: getStateByValue(address.subdivision),
    country: "ES"
  };
};

export const reuseOpencellAddresses = addresses => {
  return uniqWith(addresses, isEqual).map(mapLegacyAddress)
}
