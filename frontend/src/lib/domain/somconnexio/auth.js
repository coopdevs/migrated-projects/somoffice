export const preprocessUsernameForApi = username => {
  return `es${username.replace(/^es/i, "")}`.toLowerCase();
}

export const displayVatNumber = username => {
  return `${username.replace(/^es/i, "")}`.toUpperCase();
}
