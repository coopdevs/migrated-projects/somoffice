import { get, post } from "axios";

export const createTicket = async ({ body, subject, meta, override_ticket_ids }) => {
  return await post(`/api/tickets/`, { body, subject, meta, override_ticket_ids });
};

export const getTickets = async ({ type, ...filters }) => {
  return await get(`/api/tickets/`, {
    params: { ticket_type: type, ...filters }
  });
};
