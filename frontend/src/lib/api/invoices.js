import { get } from "axios";

export const getInvoicesList = () => get("/api/invoices/");

export const checkInvoice = async ({ id }) => {
  let result;

  try {
    await get(`/api/invoices/check/${id}`);
    result = true;
  } catch (e) {
    result = false;
    /* handle error */
  }

  return result;
};
