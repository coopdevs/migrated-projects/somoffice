import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "../src/theme";

export const decorator = Story => (
  <div className="js-wtf" style={{ border: "1px solid red" }}>
    <ThemeProvider theme={theme}>
      <Story />
    </ThemeProvider>
  </div>
);
